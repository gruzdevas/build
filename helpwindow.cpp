/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#include "helpwindow.h"
#include <QLocale>
#include <QApplication>

HelpWindow::HelpWindow()
{
  fWidget = new QFrame(this);
  setWidget(fWidget);
  toolBar = new QToolBar;
  toolBar->setObjectName("help_toolbar");
  textBrowser = new QTextBrowser;
  QVBoxLayout *vl = new QVBoxLayout;
  vl->addWidget(toolBar);
  vl->addWidget(textBrowser);
  widget()->setLayout(vl);

  home();
  toolBar->addAction(QIcon(":/images/back_16_h.png"), tr("Back"), textBrowser, SLOT(backward()));
  toolBar->addAction(QIcon(":/images/forward_16_h.png"), tr("Forward"), textBrowser, SLOT(forward()));
  toolBar->addSeparator();
  toolBar->addAction(QIcon(":/images/home_16_h.png"), tr("Home"), this, SLOT(home()));
}

void HelpWindow::hideEvent(QHideEvent *)
{
    emit windowVisibilityChanged();
}

void HelpWindow::home()
{
  textBrowser->setSource(QUrl("index.html"));
}
