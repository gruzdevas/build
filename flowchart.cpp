/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#include "flowchart.h"
#include <QtGui>
#include <QDomDocument>
#include <QApplication>

FlowChart::FlowChart(QWidget *pObj) : QWidget(pObj), fZoom(1)
{
    fBuffer = QString();
    fTargetPoint = InsertionPoint();
    fStatus = Display;
    fRoot = new Terminal();
    clear();
    setZoom(1);
}

FlowChart::~FlowChart()
{
    clear();
}

void FlowChart::makeUndo()
{
    QString state = toString();
    undoStack.push(state);
    redoStack.clear();
    emit modified();
}

bool FlowChart::canUndo() const
{
    return !undoStack.isEmpty();
}

bool FlowChart::canRedo() const
{
    return !redoStack.isEmpty();
}

bool FlowChart::canPaste() const
{
    QDomDocument doc;
    QClipboard *clp = QApplication::clipboard();
    QString aBuffer = clp->text();
    return doc.setContent(aBuffer, false);
}

void FlowChart::makeChanged()
{
    emit changed();
}

void FlowChart::undo()
{
    if (!undoStack.isEmpty())
    {
        QString state = toString();
        redoStack.push(state);
        state = undoStack.pop();
        fromString(state);
        deselectAll();
        emit changed();
    }
}


void FlowChart::redo()
{
    if (!redoStack.isEmpty())
    {
        QString state = toString();
        undoStack.push(state);
        state = redoStack.pop();
        fromString(state);
        deselectAll();
        emit changed();
    }
}


void FlowChart::clear()
{
    deselectAll();
    root()->clear();
    root()->getAttributes().clear();
    root()->setAttribute("type", "terminal");
    Block *branch = new Branch();
    branch->setAttribute("type", "branch");
    root()->append(branch);
    emit changed();
}

void FlowChart::selectAll()
{
    fActiveBlock = root();
    emit changed();
    update();
}

void FlowChart::deselectAll()
{
    fActiveBlock = nullptr;
    emit changed();
    update();
}

void FlowChart::paintEvent(QPaintEvent *pEvent)
{
    QPainter canvas(this);
    pEvent->accept();
    canvas.setClipRect(pEvent->rect());
    canvas.setRenderHint(QPainter::Antialiasing, true);
    paintTo(&canvas);
}

void FlowChart::paintTo(QPainter *canvas)
{
    if (root())
    {
        this->paint(canvas, root());
        if (status() == Insertion  || status() == Changing)
        {
            FlowChartStyle st = chartStyle();
            for (int i = 0; i < insertionPoints.size(); ++i)
            {
                InsertionPoint ip = insertionPoints.at(i);
                QPointF p = ip.point();
                canvas->setPen(QPen(st.normalForeground(), 2 * zoom()));
                canvas->setBrush(st.normalForeground());
                canvas->drawEllipse(p, 3 * zoom(), 3 * zoom());
            }
            if (!targetPoint().isNull())
            {
                QPointF p = targetPoint().point();
                canvas->setPen(QPen(st.selectedForeground(), 2 * zoom()));
                canvas->setBrush(st.selectedForeground());
                canvas->drawEllipse(p, 7 * zoom(), 7 * zoom());
            }
        }
    }
}

QDomDocument FlowChart::document() const
{
    QDomDocument doc;
    QDomElement alg = doc.createElement("algorithm");
    QDomElement r = root()->xmlNode(doc);
    QDomElement v = varsToXML(doc);
    doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\""));
    alg.appendChild(r);
    alg.appendChild(v);
    doc.appendChild(alg);
    return doc;
}

QDomElement FlowChart::varsToXML(QDomDocument & doc) const
{
    QDomElement self = doc.createElement("variables");
    for (int i = 0; i < variables.length(); ++i)
    {
        QDomElement child = doc.createElement("variable");
        child.setAttribute("name", variables.at(i)->getName());
        child.setAttribute("varType", variables.at(i)->getType());
        child.setAttribute("value", variables.at(i)->getValue());
        self.appendChild(child);
    }
    return self;
}

void FlowChart::XMLToVars(const QDomElement & node)
{
    variables.clear();
    QDomNodeList children = node.childNodes();
    for(int i = 0; i < children.size(); ++i)
    {
        if (children.at(i).isElement())
        {
            QDomElement child = children.at(i).toElement();
            if (child.nodeName() == "variable")
            {
                QDomNamedNodeMap attrs = child.attributes();
                Variable *a = new Variable;
                for (int i = 0; i < attrs.size(); ++i)
                {
                    QDomAttr da = attrs.item(i).toAttr();
                    if(da.name() != "type")
                    {
                        if(da.name() == "name")
                        {
                            a->setName(da.value());
                        }
                        else if(da.name() == "varType")
                        {
                            a->setType(da.value());
                        }
                        else if(da.name() == "value")
                        {
                            a->setValue(da.value());
                        }
                    }
                }
                variables.append(a);
            }
        }
    }
}

Block * FlowChart::root() const
{
    return fRoot;
}


Block * FlowChart::activeBlock() const
{
    return fActiveBlock;
}

void FlowChart::setZoom(const double aZoom)
{
    fZoom = aZoom;
    realignObjects();
    if (status() == Insertion  || status() == Changing)
    {
        fTargetPoint = InsertionPoint();
        regeneratePoints();
    }
    emit zoomChanged(aZoom);
}

void FlowChart::setStatus(int aStatus)
{
    fStatus = aStatus;
    if (status() == Insertion || status() == Changing)
    {
        setMouseTracking(true);
        regeneratePoints();
        update();
    }
    else
    {
        fTargetPoint = InsertionPoint();
        setMouseTracking(false);
    }
    emit statusChanged();
    emit changed();
}

void FlowChart::deleteActiveBlock()
{
    if (activeBlock())
    {
        makeUndo();
        Block *tmp = activeBlock();
        fActiveBlock = nullptr;
        deleteBlock(tmp);
        emit changed();
    }
}
void FlowChart::realignObjects()
{
    if(root())
    {
        root()->graph->adjustSize(zoom());
        root()->graph->adjustPosition(0,0);
        resize(root()->graph->width, root()->graph->height);
        emit changed();
        update();
    }
}



void FlowChart::setBuffer(const QString & aBuffer)
{
    if(!aBuffer.isEmpty())
    {
        fBuffer = aBuffer;
    }
    else
    {
        fBuffer = QString();
    }
}

double FlowChart::zoom() const
{
    return fZoom;
}

void FlowChart::mousePressEvent(QMouseEvent *pEvent)
{
    if (status() == Selectable)
    {
        Block *block = root()->blockAt(pEvent->x(), pEvent->y());
        if (block)
        {
            if (this->isBlockActive(block) && ((pEvent->modifiers() & Qt::ControlModifier) != 0))
            {
                if(activeBlock()->getParent())
                {
                    fActiveBlock = activeBlock()->getParent();
                }
                else
                {
                    fActiveBlock = block;
                }
            }
            else
            {
                if(block->getParent() !=nullptr && block->getAttribute("type") == "branch")
                {
                    fActiveBlock = block->getParent();
                }
                else
                    fActiveBlock = block;

            }
            emit changed();
            update();
        }
    }
    else if(status() == Insertion)
    {
        QPoint mp = pEvent->pos();
        InsertionPoint ip = getNearistPoint(mp.x(), mp.y());
        fTargetPoint = ip;
        if(!ip.isNull() && !buffer().isEmpty())
        {
            Block *branch = ip.branch();
            if(branch)
            {
                makeUndo(); // добавляем в стэк
                branch->insertBlock(ip.index(), buffer()); //создаем объект(ы)
                realignObjects();
                regeneratePoints();
                fActiveBlock = nullptr;
                emit changed();
            }
        }
        if (!multiInsert()) setStatus(Selectable);
    }
    else if(status() == Changing)
    {
        QPoint mp = pEvent->pos();
        InsertionPoint ip = getNearistPoint(mp.x(), mp.y());
        fTargetPoint = ip;
        if(!ip.isNull() && !buffer().isEmpty())
        {
            Block *branch = ip.branch();
            if(branch)
            {
                makeUndo(); // добавляем в стэк
                Decision *ifBlock = dynamic_cast<Decision*>(activeBlock());
                if (buffer() == "true")
                {
                        if (branch->getItems().length() > ip.index() && branch->item(ip.index())){
                            ifBlock->setNextTrue(branch->getItems().at(ip.index()));
                            ifBlock->setAttribute("true", QString::number(branch->item(ip.index())->getId()));
                        }
                        else
                        {
                            if (branch->getParent()->getAttribute("type") == "if")
                            {
                                Decision *ifPBlock = dynamic_cast<Decision*>(branch->getParent());
                                if (branch->getId() == ifPBlock->item(0)->getId())
                                    ifBlock->setNextTrue(ifPBlock->getNextTrue());
                                else
                                    ifBlock->setNextTrue(ifPBlock->getNextFalse());
                            }
                            else
                            {
                                ifBlock->setAttribute("true", "");
                                ifBlock->setNextTrue(nullptr);
                            }
                        }
                }
                else
                {
                        if (branch->getItems().length() > ip.index() && branch->item(ip.index())){
                            ifBlock->setNextFalse(branch->getItems().at(ip.index()));
                            ifBlock->setAttribute("false", QString::number(branch->item(ip.index())->getId()));
                        }
                        else
                        {
                            if (branch->getParent()->getAttribute("type") == "if")
                            {
                                Decision *ifPBlock = dynamic_cast<Decision*>(branch->getParent());
                                if (branch->getId() == ifPBlock->item(0)->getId())
                                    ifBlock->setNextFalse(ifPBlock->getNextTrue());
                                else
                                    ifBlock->setNextFalse(ifPBlock->getNextFalse());
                            }
                            else
                            {
                                ifBlock->setAttribute("false", "");
                                ifBlock->setNextFalse(nullptr);
                            }
                        }
                }

                realignObjects();
                regeneratePoints();
                fActiveBlock = nullptr;
                emit changed();
            }
        }
        setStatus(Selectable);
    }
}

void FlowChart::mouseDoubleClickEvent(QMouseEvent * event)
{
    if(status() == Selectable && event->modifiers() == Qt::NoModifier)
    {
        Block *block = root()->blockAt(event->x(), event->y());
        if (block)
        {
            emit changed();
            emit editBlock(block);
        }
    }
}

void FlowChart::mouseMoveEvent(QMouseEvent *pEvent)
{
    if(status() == Insertion || status() == Changing)
    {
        QPoint mp = pEvent->pos();
        InsertionPoint ip = getNearistPoint(mp.x(), mp.y());
        fTargetPoint = ip;
        repaint();
    }
}

QSize FlowChart::sizeHint() const
{
    if (root())
    {
        return QSize(root()->graph->width, root()->graph->height);
    }
    else return QSize();

}

void FlowChart::deleteBlock(Block *aBlock)
{
    if (aBlock == root())
    {
        for(int i = 0; i < aBlock->getItems().size(); ++i)
        {
            aBlock->item(i)->clear();
        }
        realignObjects();
    }
    else if (aBlock->getAttribute("type") == "branch")
    {
        aBlock->clear();
        realignObjects();
    }
    else
    {
        int index = aBlock->index();
        Block *par = aBlock->getParent();
        checkForNext(aBlock);
        delete aBlock;
        realignObjects();
        if (index > 0)
            par->item(index - 1)->setNext();
    }
    emit changed();
}

InsertionPoint FlowChart::getNearistPoint(int x, int y) const
{
    InsertionPoint result;
    if (insertionPoints.size() > 0)
    {
        result = insertionPoints.at(0);
        double len = calcLength(result.point(), QPointF(x, y));
        for (int i = 0; i < insertionPoints.size(); ++i)
        {
            InsertionPoint ip = insertionPoints.at(i);
            double tmp = calcLength(ip.point(), QPointF(x, y));
            if (tmp < len)
            {
                result = ip;
                len = tmp;
            }
        }
    }
    return result;
}

void FlowChart::regeneratePoints()
{
    insertionPoints.clear();
    if(root())
    {
        generatePoints(root());
    }
}

void FlowChart::generatePoints(Block *aBlock)
{
    if(aBlock->getAttribute("type") == "branch")
    {
        double x = aBlock->graph->x + aBlock->graph->width / 2.0;
        for (int i = 0; i < aBlock->getItems().size(); ++i)
        {
            double y = aBlock->item(i)->graph->y;
            InsertionPoint p;
            p.setBranch(aBlock);
            p.setPoint(QPointF(x, y));
            p.setIndex(i);
            insertionPoints.append(p);
            generatePoints(aBlock->item(i));
        }
        InsertionPoint p;
        p.setBranch(aBlock);
        if(aBlock->getItems().size() == 0)
            p.setPoint(QPointF(x, aBlock->graph->y + aBlock->graph->height / 2));
        else
            p.setPoint(QPointF(x, aBlock->graph->y + aBlock->graph->height));
        p.setIndex(aBlock->getItems().size());
        insertionPoints.append(p);
    }
    else
    {
        for (int i = 0; i < aBlock->getItems().size(); ++i)
        {
            generatePoints(aBlock->item(i));
        }
    }
}

double FlowChart::calcLength(const QPointF & p1, const QPointF & p2)
{
    return (p1.x() - p2.x()) * (p1.x() - p2.x()) + (p1.y() - p2.y()) * (p1.y() - p2.y());
}

void FlowChart::setChartStyle(const FlowChartStyle & aStyle)
{
    fStyle = aStyle;
    emit changed();
    update();
}

void FlowChart::drawBottomArrow(QPainter *canvas, const QPointF & aPoint, const QSizeF & aSize)
{
    QVector<QPointF> arrow;
    arrow << QPointF(aPoint.x() - aSize.width()/2.0, aPoint.y()-aSize.height()) <<
             aPoint << aPoint << QPointF(aPoint.x() + aSize.width()/2.0, aPoint.y()-aSize.height());
    canvas->drawLines(arrow);
}

QString FlowChart::toString()
{
    return document().toString(2);
}

void FlowChart::fromString(const QString & str)
{
    QDomDocument doc;
    if(doc.setContent(str, false))
    {
        blocks.clear();
        root()->setXmlNode(doc.firstChildElement("algorithm").firstChildElement("terminal"));
        root()->setNodesNext();
        realignObjects();
        emit changed();
    }
}

void FlowChart::drawRightArrow(QPainter *canvas, const QPointF & aPoint, const QSizeF & aSize)
{
    QVector<QPointF> arrow;
    arrow << QPointF(aPoint.x() - aSize.width(), aPoint.y() - aSize.height() / 4);
    arrow << aPoint;
    arrow << aPoint;
    arrow << QPointF(aPoint.x() - aSize.width(), aPoint.y() + aSize.height() / 4);
    canvas->drawLines(arrow);
}

void FlowChart::drawLeftArrow(QPainter *canvas, const QPointF &aPoint, const QSizeF &aSize)
{
    QVector<QPointF> arrow;
    arrow << QPointF(aPoint.x() + aSize.width(), aPoint.y() - aSize.height() / 4);
    arrow << aPoint;
    arrow << aPoint;
    arrow << QPointF(aPoint.x() + aSize.width(), aPoint.y() + aSize.height() / 4);
    canvas->drawLines(arrow);
}

/******************************** Block ***********************************/

int Block::lastID = 1;

Block::Block()
{
    parent = nullptr;
    attributes.clear();
    items.clear();
    parent = nullptr;
    BlockGraphics *graph = new BlockGraphics();
    graph->block = this;
    this->graph = graph;
}


Block::~Block()
{
    if(parent != nullptr)
    {
        parent->remove(this);
        clear();
    }
}

BlockGraphics::BlockGraphics()
{
    topMargin = 0;
    bottomMargin = 0;
    leftMargin = 0;
    rightMargin = 0;
    x = 0;
    y = 0;
    width = 0;
    height = 0;
}

int Block::index()
{

    if(parent == nullptr) return -1;
    else return parent->getItems().indexOf(this);
}

void Block::insert(int newIndex, Block *aBlock)
{
    if (aBlock->parent != nullptr)
    {
        aBlock->parent->remove(aBlock);
    }
    if (newIndex < 0 || newIndex >= items.size())
        items.append(aBlock);
    else
        items.insert(newIndex, aBlock);
    aBlock->parent = this;
}

void Block::remove(Block *aBlock)
{
    items.removeAll(aBlock);
    aBlock->parent = nullptr;
}

void Block::append(Block *aBlock)
{
    insert(-1, aBlock);
}

void Block::deleteObject(int aIndex)
{
    Block *tmp = item(aIndex);
    if(tmp)
    {
        delete tmp;
    }
}


void Block::setItem(int aIndex, Block *aBlock)
{
    if(items.size() > aIndex && aIndex >= 0)
    {
        if (aBlock->parent != nullptr)
        {
            aBlock->parent->remove(aBlock);
        }
        Block *old = item(aIndex);
        old->parent = nullptr;
        items.replace(aIndex, aBlock);
    }
}

void Block::clear()
{
    while(!items.isEmpty())
    {
        deleteObject(0);
    }
    QString currentType = getAttribute("type");
    attributes.clear();
    setAttribute("type", currentType);
    items.clear();
}

void BlockGraphics::adjustSize(const double aZoom)
{
    double clientWidth = 0, clientHeight = 0;
    if (block->getAttribute("type") == "branch")
    {
        for (int i = 0; i < block->getItems().size(); ++i)
        {
            block->item(i)->graph->adjustSize(aZoom);
            if (clientWidth < block->item(i)->graph->width) clientWidth = block->item(i)->graph->width;
            clientHeight += block->item(i)->graph->height;
        }
        double minWidth = 180 * aZoom;
        double minHeight = 15 * aZoom;
        if (clientHeight < minHeight) clientHeight = minHeight;
        if (clientWidth < minWidth) clientWidth = minWidth;
        height = clientHeight;
        width = clientWidth;

    }
    else
    {
        for (int i = 0; i < block->getItems().size(); ++i)
        {

            block->item(i)->graph->adjustSize(aZoom);
            if (clientHeight < block->item(i)->graph->height) clientHeight = block->item(i)->graph->height;
            clientWidth += block->item(i)->graph->width;
        }
        /* поля по умолчанию */
        topMargin = 60 * aZoom;
        bottomMargin = 60 * aZoom;
        leftMargin = 10 * aZoom;
        rightMargin = 10 * aZoom;
        if (block->getAttribute("type") == "terminal")
        {
            topMargin = 40 * aZoom;
            bottomMargin = 50 * aZoom;
        }
        else if (block->getAttribute("type") == "process")
        {
            topMargin = 16 * aZoom;
            bottomMargin = 10 * aZoom;
            clientWidth = 120 * aZoom;
            clientHeight = 60 * aZoom;
        }
        else if (block->getAttribute("type") == "io" || block->getAttribute("type") == "ou")
        {
            topMargin = 16 * aZoom;
            bottomMargin = 10 * aZoom;
            leftMargin = 20 * aZoom;
            rightMargin = 20 * aZoom;
            clientWidth = 120 * aZoom;
            clientHeight = 60 * aZoom;
        }
        else if (block->getAttribute("type") == "if")
        {
            topMargin = 92 * aZoom;
            bottomMargin = 16 * aZoom;
        }

        width = leftMargin + clientWidth + rightMargin;
        height = topMargin + clientHeight + bottomMargin;
    }
}

void BlockGraphics::adjustPosition(const double ox, const double oy)
{
    x = ox; // Поискать другую отрисовку блоков
    y = oy;
    if (block->getAttribute("type") == "branch")
    {
        double cy = y;
        for (int i = 0; i < block->getItems().size(); ++i)
        {
            block->item(i)->graph->adjustPosition(ox + (width - block->item(i)->graph->width) / 2, cy);
            cy += block->item(i)->graph->height;
        }
    }
    else
    {
        double cx = x + leftMargin;
        for (int i = 0; i < block->getItems().size(); ++i)
        {
            block->item(i)->graph->adjustPosition(cx, y + topMargin);
            cx += block->item(i)->graph->width;
        }
    }
}


Block * Block::blockAt(int px, int py)
{
    QRectF rect(graph->x, graph->y, graph->width, graph->height);
    if (!rect.contains(px, py))
    {
        return nullptr;
    }
    else
    {
        for (int i = 0; i < items.size(); ++i)
        {
            Block *tmp = item(i)->blockAt(px, py);
            if (tmp) return tmp;
        }
        return this;
    }
}

QDomElement Block::xmlNode(QDomDocument & doc) const
{
    QDomElement self = doc.createElement(getAttribute("type"));
    QList<QString> sl = attributes.uniqueKeys();
    if (getId() != 0)
        self.setAttribute("id", getId());
    for (int i = 0; i < sl.size(); ++i)
    {
        if (sl.at(i) != "type")
        {
            self.setAttribute(sl.at(i), attributes.value(sl.at(i), QString()));
        }
    }


    for (int i = 0; i < items.size(); ++i)
    {
        QDomElement child = item(i)->xmlNode(doc);
        self.appendChild(child);
    }
    return self;
}


Block *Decision::execute(){
    QStringList condition = this->getAttribute("cond").split(QRegExp("\\s+"), QString::SkipEmptyParts);
    if (condition.length() < 3)
        throw(0x04);
    QString sign = condition[1];
    QString firstEl = condition[0];
    QString lastEl = condition[2];
    double eps = 0.000001;
    double first = 0, last = 0;
    QStringList signs = {"==", "!=", "<", "<=", ">", ">="};
    Block *pointer = nullptr;
    int firstIndex = -1;
    for (int i = 0; i < variables.length(); i++){
        if (variables.at(i)->getName() == firstEl){
            firstIndex = i;
            firstEl = variables.at(firstIndex)->getValue();
            break;
        }
    }
    if (firstIndex == -1)
        throw(0x01); // Если нет - конец программы (неверный ввод)
    int secondIndex = -1;
    bool isSecondVar = true;
    for (int i = 0; i < variables.length(); i++){
        if (variables.at(i)->getName() == lastEl){
            secondIndex = i;
            lastEl = variables.at(secondIndex)->getValue();
            break;
        }
    }
    if (secondIndex == -1) {
        isSecondVar = false;

    }
    bool areStrings, areNumbers;
    if (isSecondVar){
        if (variables.at(firstIndex)->getType() != variables.at(secondIndex)->getType())
            return nullptr; // +ошибка
        areStrings = (variables.at(firstIndex)->getType() == "String" && variables.at(secondIndex)->getType() == "String");
        areNumbers = (variables.at(firstIndex)->getType() == "Double" && variables.at(secondIndex)->getType() == "Double");
    }
    else {
        bool flag;
        last = lastEl.toDouble(&flag);
        if(!flag){
            if (variables.at(firstIndex)->getType() == "Double")
            {
                throw(0x05);
            }
            else
            {
                areNumbers = false;
                areStrings = true;
            }
        }
        else{
            if (variables.at(firstIndex)->getType() == "Double")
            {
                areNumbers = true;
                areStrings = false;
            }
            else
            {
                throw(0x05);
            }
        }
    }
    if (areNumbers){
        bool flag;
        first = firstEl.toDouble(&flag);
        if(!flag){
            return nullptr; // +ошибка
        }
        last = lastEl.toDouble(&flag);
        if(!flag){
            return nullptr; // +ошибка
        }
    }
    int signIndex = signs.indexOf(sign);
    if (signIndex != -1){
        switch (signIndex) {
        case 0:
            if (areStrings){
                if (firstEl == lastEl) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            else if (areNumbers){
                if (abs(first - last) < eps) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            break;

        case 1:
            if (areStrings){
                if (firstEl != lastEl) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            else if (areNumbers){
                if (abs(first - last) > eps) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            break;
        case 2:
            if (areStrings){
                if (firstEl < lastEl) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            else if (areNumbers){
                if (first < last) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            break;
        case 3:
            if (areStrings){
                if (firstEl <= lastEl) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            else if (areNumbers){
                if (first < last || abs(first - last) < eps) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            break;
        case 4:
            if (areStrings){
                if (firstEl > lastEl) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            else if (areNumbers){
                if (first > last) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            break;
        case 5:
            if (areStrings){
                if (firstEl >= lastEl) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            else if (areNumbers){
                if (first > last || abs(first - last) < eps) {
                    if (this->item(0)->getItems().length() > 0)
                        return this->item(0)->item(0);
                    else
                        pointer = this->nextTrue;
                }
                else {
                    if (this->item(1)->getItems().length() > 0)
                        return this->item(1)->item(0);
                    else
                        pointer = this->nextFalse;
                }
            }
            break;
        }
    }
    else return nullptr;

    if (pointer == nullptr){
        if (this->getParent()->getParent()->getAttribute("type") == "if"){
            Decision *ifBlock = dynamic_cast<Decision*>(this->getParent()->getParent());
            if (this->getParent()->index() == 0)
                return ifBlock->getNextTrue();
            else
                return ifBlock->getNextFalse();
        }
    }
    return pointer;
}

Block *Output::execute(){
    QString variable = this->getAttribute("var");
    int firstIndex = -1;
    for (Variable *v : variables){
        if (v->getName() == variable)
        {
            firstIndex = variables.indexOf(v);
            variable = v->getValue();
            break;
        }
    }
    if (firstIndex == -1){
        throw(0x01);
    }
    console->console->output(variable);
    if (console->exec() == QDialog::Accepted){

    }
    if (this->next == nullptr){
        if (this->getParent()->getParent()->getAttribute("type") == "if"){
            Decision *ifBlock = dynamic_cast<Decision*>(this->getParent()->getParent());
            if (this->getParent()->index() == 0) return ifBlock->getNextTrue();
            else return ifBlock->getNextFalse();
        }
    }
    return this->next;
}

Block *Input::execute(){
    QString variable = this->getAttribute("var");
    int firstIndex = -1;
    for (Variable *v : variables){
        if (v->getName() == variable)
        {
            firstIndex = variables.indexOf(v);
            break;
        }
    }
    if (firstIndex == -1) {
        throw(0x01);
    }
    console->console->setupInput();
    if (console->exec() == QDialog::Accepted){
        variables.at(firstIndex)->setValue(console->console->read());
    }
    if (this->next == nullptr){
        if (this->getParent()->getParent()->getAttribute("type") == "if"){
            Decision *ifBlock = dynamic_cast<Decision*>(this->getParent()->getParent());
            if (this->getParent()->index() == 0) return ifBlock->getNextTrue();
            else return ifBlock->getNextFalse();
        }
    }
    return this->next;
}

Block *Process::execute(){
    QStringList signs = {"+", "-", "*", "/"};
    QString variable = this->getAttribute("variable");
    QString firstEl  = this->getAttribute("first");
    QString sign     = this->getAttribute("sign");
    QString lastEl   = this->getAttribute("second");
    int firstIndex = -1, secondIndex = -1, thirdIndex = -1;
    double secondValue = 0, thirdValue = 0;
    bool isFirstNum = true, isSecondVar = true, isThirdVar = true;
    for (Variable *v : variables){
        if (v->getName() == variable)
        {
            firstIndex = variables.indexOf(v);
            variable = v->getValue();
            isFirstNum = (v->getType() == "Double");
            break;
        }
    }
    if (firstIndex == -1) {
        throw(0x01);
    }
    for (Variable *v : variables){
        if (v->getName() == firstEl)
        {
            secondIndex = variables.indexOf(v);
            firstEl = v->getValue();
            break;
        }
    }
    if (secondIndex == -1) {
        isSecondVar = false;
    }
    for (Variable *v : variables){
        if (v->getName() == lastEl)
        {
            thirdIndex = variables.indexOf(v);

            lastEl = v->getValue();
            break;
        }
    }
    if (thirdIndex == -1) {
        isThirdVar = false;
    }
    if (!isSecondVar){
        if (isFirstNum){
            bool flag;
            secondValue = firstEl.toDouble(&flag);
            if(!flag){
                throw(0x02);
            }
        }
    }
    else
        if(variables.at(secondIndex)->getType() == "Double" && isFirstNum){
            bool flag;
            secondValue = firstEl.toDouble(&flag);
            if(!flag){
                throw(0x02);
            }
        }
    if (!isThirdVar){
        if (isFirstNum && lastEl!=""){
            bool flag;
            thirdValue = lastEl.toDouble(&flag);
            if(!flag){
                throw(0x03);
            }
        }
    }
    else
        if(variables.at(thirdIndex)->getType() == "Double" && isFirstNum){
            bool flag;
            thirdValue = lastEl.toDouble(&flag);
            if(!flag){
                throw(0x03);
            }
        }

    int signIndex = signs.indexOf(sign);
    switch (signIndex) {
    case 0: // +
        if (!isFirstNum)
            variables.at(firstIndex)->setValue(firstEl.append(lastEl));
        else if (isFirstNum){
            variables.at(firstIndex)->setValue(QString("%1").arg(secondValue + thirdValue));
        }
        break;
    case 1: // -
        if (!isFirstNum)
            variables.at(firstIndex)->setValue(firstEl.remove(lastEl));
        else if (isFirstNum){
            variables.at(firstIndex)->setValue(QString("%1").arg(secondValue - thirdValue));
        }
        break;
    case 2: // *
        if (!isFirstNum)
            break;
        else if (isFirstNum){
            variables.at(firstIndex)->setValue(QString("%1").arg(secondValue * thirdValue));
        }
        break;
    case 3: // /
        if (!isFirstNum)
            break;
        else if (isFirstNum){
            variables.at(firstIndex)->setValue(QString("%1").arg(secondValue / thirdValue));
        }
        break;
    case -1:
        if (!isFirstNum){
            variables.at(firstIndex)->setValue(firstEl);
        }
        else {
            variables.at(firstIndex)->setValue(QString("%1").arg(secondValue));
        }
        break;

    }
    if (this->next == nullptr){
        if (this->getParent()->getParent() && this->getParent()->getParent()->getAttribute("type") == "if"){
            Decision *ifBlock = dynamic_cast<Decision*>(this->getParent()->getParent());
            if (this->getParent()->index() == 0) return ifBlock->getNextTrue();
            else return ifBlock->getNextFalse();
        }
    }
    return this->next;
}

void Decision::setNext(){
    Block *nextTrue = nullptr;
    Block *nextFalse = nullptr;
    bool changeTrue = true, changeFalse = true;
    if (this->nextTrue)
        if ( this->nextTrue->getParent() != this->getParent() || this->nextTrue->index() != this->index() + 2)
            changeTrue = false;
    if (this->nextFalse)
        if ( this->nextFalse->getParent() != this->getParent() || this->nextFalse->index() != this->index() + 2)
            changeFalse = false;
    if (index() + 1 < this->getParent()->getItems().length()) {
        if (changeTrue)
            nextTrue = this->getParent()->item(index() + 1);
        if (changeFalse)
            nextFalse = this->getParent()->item(index() + 1);
    }
    if (changeTrue)
        this->nextTrue = nextTrue;
    if (changeFalse)
        this->nextFalse = nextFalse;
    if (index() - 1 >= 0) {
        this->getParent()->item(index() - 1)->setNext();
    }
}
void Input::setNext(){
    Block *next = nullptr;
    if (index() + 1 < this->getParent()->getItems().length()) {
        next = this->getParent()->getItems().at(index() + 1);
    }
    this->next = next;
    if (index() - 1 >= 0) {
        this->getParent()->item(index() - 1)->setNext();
    }
}
void Output::setNext(){
    Block *next = nullptr;
    if (index() + 1 < this->getParent()->getItems().length()) {
        next = this->getParent()->getItems().at(index() + 1);
    }
    this->next = next;
    if (index() - 1 >= 0) {
        this->getParent()->item(index() - 1)->setNext();
    }
}
void Process::setNext(){
    Block *next = nullptr;
    if (index() + 1 < this->getParent()->getItems().length()) {
        next = this->getParent()->getItems().at(index() + 1);
    }
    this->next = next;
    if (index() - 1 >= 0) {
        this->getParent()->item(index() - 1)->setNext();
    }
}


void Block::setXmlNode(const QDomElement & node)
{
    clear();
    setAttribute("type", node.nodeName());
    QDomNamedNodeMap attrs = node.attributes();
    QDomNodeList children = node.childNodes();

    for (int i = 0; i < attrs.size(); ++i)
    {
        QDomAttr da = attrs.item(i).toAttr();
        if(da.name() != "type")
        {
            attributes.insert(da.name(), da.value());
        }
    }
    id = attributes.value("id", QString()).toInt();
    if (lastID < id) lastID = id;

    for(int i = 0; i < children.size(); ++i)
    {
        if (children.at(i).isElement())
        {
            QDomElement child = children.at(i).toElement();
            Block *block = nullptr; // в зависимоти от типа создается объект
            if (child.nodeName() == "if")
            {
                block = new Decision();
            }
            else if (child.nodeName() == "process")
            {
                block = new Process();
            }
            else if (child.nodeName() == "io")
            {
                block = new Input();
            }
            else if (child.nodeName() == "ou")
            {
                block = new Output();
            }
            else if (child.nodeName() == "branch")
            {
                block = new Branch();
            }
            block->setXmlNode(child);
            blocks.append(block);
            append(block);
//            if (child.nodeName() != "branch")
//                block->setNext();
//            if (block->getAttribute("true") != "")
//                if (Decision *ifBlock = dynamic_cast<Decision*>(block))
//                    ifBlock->setNextTrue(getBlockByID(block->getAttribute("true").toInt()));
//            if (block->getAttribute("false") != "")
//                if (Decision *ifBlock = dynamic_cast<Decision*>(block))
//                    ifBlock->setNextFalse(getBlockByID(block->getAttribute("false").toInt()));
        }
    }
}

void Block::setNodesNext(){
    if (this->getAttribute("type") != "branch" && this->getAttribute("type") != "terminal")
        this->setNext();
    if (this->getAttribute("true") != "")
        if (Decision *ifBlock = dynamic_cast<Decision*>(this))
            ifBlock->setNextTrue(getBlockByID(this->getAttribute("true").toInt()));
    if (this->getAttribute("false") != "")
        if (Decision *ifBlock = dynamic_cast<Decision*>(this))
            ifBlock->setNextFalse(getBlockByID(this->getAttribute("false").toInt()));
    for(int i = 0; i < getItems().size(); ++i)
    {
        item(i)->setNodesNext();
    }
}


void Block::addSetup(QString type)
{
    clear();
    setAttribute("type", type);
    id = ++lastID;
    setAttribute("id", QString::number(id));
    if (getAttribute("type") == "if"){
        Block *trueBranch = new Branch();
        Block *falseBranch = new Branch();

        trueBranch->addSetup("branch");
        falseBranch->addSetup("branch");
        append(trueBranch);
        append(falseBranch);
    }

}


void Block::insertBlock(int aIndex, QString type)
{
    if (getAttribute("type") == "branch")
    {
        int ind = aIndex;
        Block *block = nullptr; // в зависимоти от типа создается объект
        if (type == "if")
        {
            block = new Decision();
        }
        else if (type == "process")
        {
            block = new Process();
        }
        else if (type == "io")
        {
            block = new Input();
        }
        else if (type == "ou")
        {
            block = new Output();
        }
        blocks.append(block);
        block->addSetup(type); // Аттрибуты и создание дочерних элементов
        insert(ind, block);
        block->setNext();
        ind++;
    }
}

bool FlowChart::isBlockActive(Block *block)
{
    if (this->activeBlock() == block) return true;
    else if (block->getParent())
    {
        return this->isBlockActive(block->getParent());
    }
    else
    {
        return false;
    }
}

bool FlowChart::isThereOutput(Block *block)
{
    if (block->getAttribute("type") == "ou") return true;
    for (int i = 0; i < block->getItems().length(); i ++)
    {
        if (isThereOutput(block->item(i))) return true;
    }
    return false;
}

void FlowChart::paint(QPainter *canvas, Block *block)
{
    FlowChartStyle st = this->chartStyle();
    double hcenter = block->graph->x + block->graph->width / 2;
    /* в соответствии с ГОСТ 19.003-80 */
    double a = 60 * this->zoom();
    double b = 2 * a;
    double bottom = block->graph->y + block->graph->height;
    double lw = st.lineWidth() * this->zoom();
    double const zoom = this->zoom();
    double y = block->graph->y;
    double x = block->graph->x;
    double height = block->graph->height;
    double width = block->graph->width;
    QFont font("Tahoma");
    font.setWeight(0);

    font.setPixelSize(13 * this->zoom());
    font = QFont(font, canvas->device());
    canvas->setFont(font);

    if(this->status() == FlowChart::Selectable && this->isBlockActive(block))
    {
        canvas->setPen(QPen(st.selectedForeground(), lw, Qt::DashLine));
        canvas->setBrush(QBrush(st.normalBackground()));
    }
    else
    {
        canvas->setPen(QPen(st.normalForeground(), lw, Qt::SolidLine));
        canvas->setBrush(QBrush(st.normalBackground()));
    }

    QPen pen = canvas->pen();
    pen.setCapStyle(Qt::FlatCap);
    pen.setJoinStyle(Qt::MiterJoin);
    canvas->setPen(pen);
    canvas->fillRect(QRectF(x, y, width, height), canvas->brush());

    if (block->getAttribute("type") == "branch")
    {
        /* отрисовка ветви */
        QLineF line(hcenter, y-0.5, hcenter, y + height+0.5);
        canvas->drawLine(line);
    }
    else
    {
        if (block->getAttribute("type") == "terminal")
        {
            /* алгоритм */
            Block *body = block->item(0);
            QRectF oval(hcenter - b/2, lw, b, a/2);
            canvas->drawRoundedRect(oval, a/4, a/4);
            canvas->drawText(oval, Qt::TextSingleLine | Qt::AlignHCenter | Qt::AlignVCenter, "Начало");
            canvas->drawLine(QLineF(hcenter, y + a/2 + lw, hcenter, body->graph->y+0.5));
            canvas->drawLine(QLineF(hcenter, body->graph->y + body->graph->height-0.5, hcenter, bottom - a/2-lw));
            FlowChart::drawBottomArrow(canvas, QPointF(hcenter, bottom - a/2 - lw),
                                       QSize(6 * zoom, 12 * zoom));

            oval = QRectF(hcenter - b/2, bottom - a/2 - lw, b, a/2);
            canvas->drawRoundedRect(oval, a/4, a/4);
            canvas->drawText(oval, Qt::TextSingleLine | Qt::AlignHCenter | Qt::AlignVCenter, "Конец");

        }
        else if(block->getAttribute("type") == "process")
        {
            canvas->drawLine(QLineF(hcenter, y-0.5, hcenter, y + 16 * zoom));
            FlowChart::drawBottomArrow(canvas, QPointF(hcenter, y + 16 * zoom),
                                       QSize(6 * zoom, 12 * zoom));
            QRectF rect(hcenter - b/2, y + 16 * zoom, b, a);
            QRectF textRect(hcenter - b/2 + 4 * zoom, y + 20 * zoom, b - 8 * zoom, a - 8 * zoom);
            canvas->drawRect(rect);
            if (block->getAttributes().value("variable", "") != nullptr){
                if (block->getAttributes().value("sign", "") != nullptr)
                {
                    canvas->drawText(textRect, Qt::AlignCenter | Qt::TextWrapAnywhere, QString("%1 = %2 %3 %4").arg(block->getAttribute("variable"),
                                                                                                                    block->getAttribute("first"),
                                                                                                                    block->getAttribute("sign"),
                                                                                                                    block->getAttribute("second")));
                }
                else
                {
                    canvas->drawText(textRect, Qt::AlignCenter | Qt::TextWrapAnywhere, QString("%1 = %2").arg(block->getAttribute("variable"),
                                                                                                              block->getAttribute("first")));
                }
            }
            canvas->drawLine(QLineF(hcenter, y + 16 * zoom+a, hcenter, bottom+0.5));
        }
        else if(block->getAttribute("type") == "if")
        {
            canvas->drawLine(QLineF(hcenter, y-0.5, hcenter, y + 16 * zoom));
            FlowChart::drawBottomArrow(canvas, QPointF(hcenter, y + 16 * zoom),
                                       QSize(6 * zoom, 12 * zoom));
            QPointF par[4];
            par[0] = QPointF(hcenter - b/2, y + 16 * zoom + a/2);
            par[1] = QPointF(hcenter      , y + 16 * zoom      );
            par[2] = QPointF(hcenter + b/2, y + 16 * zoom + a/2);
            par[3] = QPointF(hcenter      , y + 16 * zoom + a  );
            canvas->drawPolygon(par, 4);
            QRectF textRect(hcenter - b/2 + a/4 + 20, y + 16 * zoom + 4 + a/8, b - a/2 - 40, a - 8 - a/4);
            canvas->drawText(textRect, Qt::AlignCenter | Qt::TextWrapAnywhere, QString("%1").arg(block->getAttribute("cond")));
            Block *left = block->item(0);
            Block *right = block->item(1);
            // левая линия
            QPointF line[3];
            line[0] = QPointF(hcenter - b/2, y + 16 * zoom + a/2);
            line[1] = QPointF(left->graph->x+left->graph->width/2, y + 16 * zoom + a/2);
            line[2] = QPointF(left->graph->x+left->graph->width/2, left->graph->y);
            canvas->drawPolyline(line, 3);

            canvas->drawText(QPointF(hcenter - b/2 - 24*zoom, y + 12 * zoom + a/2), "Да");

            // правая линия
            line[0] = QPointF(hcenter + b/2, y + 16 * zoom + a/2);
            line[1] = QPointF(right->graph->x+right->graph->width/2, y + 16 * zoom + a/2);
            line[2] = QPointF(right->graph->x+right->graph->width/2, right->graph->y);
            canvas->drawPolyline(line, 3);
            canvas->drawText(QPointF(hcenter + b/2 +5*zoom, y + 12 * zoom + a/2), "Нет");

            // соединение
            Decision *ifBlock = dynamic_cast<Decision*>(block);
            QPointF collector[5];
            if (ifBlock->getNextTrue() != nullptr){
                if (ifBlock->getNextTrue()->index() != ifBlock->index() + 1 || ifBlock->getNextTrue()->getParent() != ifBlock->getParent()){
                    collector[0] = QPointF(left->graph->x + left->graph->width / 2, left->graph->y+left->graph->height);
                    collector[1] = QPointF(left->graph->x + left->graph->width / 2, left->graph->y+left->graph->height + 8 * zoom);
                    collector[2] = QPointF(left->graph->x - 2 * zoom, left->graph->y+left->graph->height + 8 * zoom);
                    collector[3] = QPointF(left->graph->x - 2 * zoom,
                                           ifBlock->getNextTrue()->graph->y);
                    collector[4] = QPointF(ifBlock->getNextTrue()->graph->x + ifBlock->getNextTrue()->graph->width / 2,
                                           ifBlock->getNextTrue()->graph->y);
                    canvas->drawPolyline(collector, 5);

                    FlowChart::drawRightArrow(canvas, collector[4],
                            QSize(6 * zoom, 12 * zoom));
                }
                else {
                    collector[0] = QPointF(left->graph->x + left->graph->width / 2, left->graph->y+left->graph->height);
                    collector[1] = QPointF(left->graph->x + left->graph->width / 2, bottom - 8 * zoom);
                    collector[2] = QPointF(hcenter, bottom - 8 * zoom);
                    collector[3] = QPointF(hcenter, bottom + 0.5);
                    canvas->drawPolyline(collector, 4);
                }
            }
            else {
                if (ifBlock->index() + 1 < ifBlock->getParent()->getItems().length()){
                    collector[0] = QPointF(left->graph->x + left->graph->width / 2, left->graph->y+left->graph->height);
                    collector[1] = QPointF(left->graph->x + left->graph->width / 2, left->graph->y+left->graph->height + 8 * zoom);
                    collector[2] = QPointF(left->graph->x - 2 * zoom, left->graph->y+left->graph->height + 8 * zoom);
                    collector[3] = QPointF(left->graph->x - 2 * zoom,
                                           ifBlock->getParent()->getParent()->graph->y + ifBlock->getParent()->getParent()->graph->width - 8 * zoom);
                    collector[4] = QPointF(ifBlock->getParent()->getParent()->graph->x + ifBlock->getParent()->getParent()->graph->width / 2,
                                           ifBlock->getParent()->getParent()->graph->y + ifBlock->getParent()->getParent()->graph->width - 8 * zoom);
                    canvas->drawPolyline(collector, 5);

                    FlowChart::drawRightArrow(canvas, collector[4],
                            QSize(6 * zoom, 12 * zoom));
                }
                else {
                    collector[0] = QPointF(left->graph->x + left->graph->width / 2, left->graph->y+left->graph->height);
                    collector[1] = QPointF(left->graph->x + left->graph->width / 2, bottom - 8 * zoom);
                    collector[2] = QPointF(hcenter, bottom - 8 * zoom);
                    collector[3] = QPointF(hcenter, bottom + 0.5);
                    canvas->drawPolyline(collector, 4);
                }

            }
            if (ifBlock->getNextFalse() != nullptr){
                if (ifBlock->getNextFalse()->index() != ifBlock->index() + 1 || ifBlock->getNextFalse()->getParent() != ifBlock->getParent()){
                    collector[0] = QPointF(right->graph->x + right->graph->width / 2, right->graph->y+right->graph->height);
                    collector[1] = QPointF(right->graph->x + right->graph->width / 2, right->graph->y+right->graph->height + 8 * zoom);
                    collector[2] = QPointF(right->graph->x + right->graph->width + 2 * zoom, right->graph->y+right->graph->height + 8 * zoom);
                    collector[3] = QPointF(right->graph->x + right->graph->width + 2 * zoom,
                                           ifBlock->getNextFalse()->graph->y);
                    collector[4] = QPointF(ifBlock->getNextFalse()->graph->x + ifBlock->getNextFalse()->graph->width / 2,
                                           ifBlock->getNextFalse()->graph->y);
                    canvas->drawPolyline(collector, 5);

                    FlowChart::drawLeftArrow(canvas, collector[4],
                            QSize(6 * zoom, 12 * zoom));
                }
                else {
                    collector[0] = QPointF(hcenter, bottom + 0.5);
                    collector[1] = QPointF(hcenter, bottom - 8 * zoom);
                    collector[2] = QPointF(right->graph->x + right->graph->width / 2, bottom - 8 * zoom);
                    collector[3] = QPointF(right->graph->x + right->graph->width / 2, right->graph->y+right->graph->height);
                    canvas->drawPolyline(collector, 4);
                }
            }
            else {

                if (ifBlock->index() + 1 < ifBlock->getParent()->getItems().length()){
                    collector[0] = QPointF(right->graph->x + right->graph->width / 2, right->graph->y+right->graph->height);
                    collector[1] = QPointF(right->graph->x + right->graph->width / 2, right->graph->y+right->graph->height + 8 * zoom);
                    collector[2] = QPointF(right->graph->x + right->graph->width + 2 * zoom, right->graph->y+right->graph->height + 8 * zoom);
                    collector[3] = QPointF(right->graph->x + right->graph->width + 2 * zoom,
                                           ifBlock->getParent()->graph->y + ifBlock->getParent()->graph->width - 8 * zoom);
                    collector[4] = QPointF(ifBlock->getParent()->graph->x + ifBlock->getParent()->graph->width / 2,
                                           ifBlock->getParent()->graph->y + ifBlock->getParent()->graph->width - 8 * zoom);
                    canvas->drawPolyline(collector, 5);

                    FlowChart::drawLeftArrow(canvas, collector[4],
                            QSize(6 * zoom, 12 * zoom));
                }
                else {
                    collector[0] = QPointF(hcenter, bottom + 0.5);
                    collector[1] = QPointF(hcenter, bottom - 8 * zoom);
                    collector[2] = QPointF(right->graph->x + right->graph->width / 2, bottom - 8 * zoom);
                    collector[3] = QPointF(right->graph->x + right->graph->width / 2, right->graph->y+right->graph->height);
                    canvas->drawPolyline(collector, 4);
                }

            }
            //canvas->drawLine(QLineF(hcenter, bottom-8*zoom, hcenter, bottom+0.5));
        }
        else if(block->getAttribute("type") == "io")
        {
            canvas->drawLine(QLineF(hcenter, y-0.5, hcenter, y + 16 * zoom));
            FlowChart::drawBottomArrow(canvas, QPointF(hcenter, y + 16 * zoom),
                                       QSize(6 * zoom, 12 * zoom));
            QPointF par[4];
            par[0] = QPointF(hcenter - b/2 + a/4, y + 16 * zoom);
            par[1] = QPointF(hcenter + b/2, y + 16 * zoom);
            par[2] = QPointF(hcenter + b/2 - a/4, y + 16 * zoom + a);
            par[3] = QPointF(hcenter - b/2, y + 16 * zoom + a);
            canvas->drawPolygon(par, 4);
            QRectF rect(hcenter - b/2, y + 16 * zoom, b, a);
            QString ls = block->getAttribute("var");
            canvas->drawText(rect, Qt::AlignCenter | Qt::TextWrapAnywhere, "Ввод " + ls);
            canvas->drawLine(QLineF(hcenter, y + 16 * zoom+a, hcenter, bottom+0.5));
        }
        else if(block->getAttribute("type") == "ou")
        {
            canvas->drawLine(QLineF(hcenter, y-0.5, hcenter, y + 16 * zoom));
            FlowChart::drawBottomArrow(canvas, QPointF(hcenter, y + 16 * zoom),
                                       QSize(6 * zoom, 12 * zoom));
            QPointF par[4];
            par[0] = QPointF(hcenter - b/2 + a/4, y + 16 * zoom);
            par[1] = QPointF(hcenter + b/2, y + 16 * zoom);
            par[2] = QPointF(hcenter + b/2 - a/4, y + 16 * zoom + a);
            par[3] = QPointF(hcenter - b/2, y + 16 * zoom + a);
            canvas->drawPolygon(par, 4);
            QRectF rect(hcenter - b/2, y + 16 * zoom, b, a);
            QString ls = block->getAttribute("var");
            canvas->drawText(rect, Qt::AlignCenter | Qt::TextWrapAnywhere, "Вывод " + ls);
            canvas->drawLine(QLineF(hcenter, y + 16 * zoom+a, hcenter, bottom+0.5));
        }
    }
    for(int i = 0; i < block->getItems().size(); ++i)
    {
        this->paint(canvas, block->item(i));
    }
}

void checkForNext (Block *block){
    for (int i = 0; i < blocks.length(); i++) {
        Block *bk = blocks.at(i);
        if(bk->getAttribute("type") == "if"){

            Decision *ifBlock = dynamic_cast<Decision*>(bk);
            if (ifBlock->getNextTrue() == block)
            {
                if (ifBlock->index() + 1 < ifBlock->getParent()->getItems().size())
                    ifBlock->setNextTrue(ifBlock->getParent()->item(ifBlock->index() + 1));
                else
                    ifBlock->setNextTrue(nullptr);
            }
            if (ifBlock->getNextFalse() == block)
            {
                if (ifBlock->index() + 1 < ifBlock->getParent()->getItems().size())
                    ifBlock->setNextFalse(ifBlock->getParent()->item(ifBlock->index() + 1));
                else
                    ifBlock->setNextFalse(nullptr);

            }
        }
    }
}
