/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#include <QtGui>
#include <QtSvg>
#include <QSettings>
#include <QWidgetList>
#include "mainwindow.h"
#include "flowchart.h"
#include "flowchartstyle.h"

#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

void AlgScrollArea::mousePressEvent(QMouseEvent *event)
{
    event->accept();
    emit mouseDown();
}

void AlgScrollArea::wheelEvent(QWheelEvent *event)
{
    if((event->modifiers() & Qt::ControlModifier) != 0) {
        event->ignore();
        emit zoomStepped(event->delta() / 120);
    }
    else {
        if((event->modifiers() & Qt::ShiftModifier) == 0) {
            QScrollBar * vsb = verticalScrollBar();
            if (vsb != nullptr) {
                /* scroll vertically */
                vsb->setValue(vsb->value() - event->delta());
            }
        }
        else {
            QScrollBar * gsb = horizontalScrollBar();
            if (gsb != nullptr) {
                /* scroll horizontally */
                gsb->setValue(gsb->value() - event->delta());
            }
        }
        event->accept();
    }
}


MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags), fDocument(nullptr)
{
    setupUi();
    readSettings();
    setNames();

    FlowChart *fc = new FlowChart(this);
    setDocument(fc);
    document()->setZoom(1);
    connect(document(), SIGNAL(statusChanged()), this, SLOT(slotStatusChanged()));
    connect(document(), SIGNAL(editBlock(Block *)), this, SLOT(slotEditBlock(Block *)));
    connect(actUndo, SIGNAL(triggered()), document(), SLOT(undo()));
    connect(actRedo, SIGNAL(triggered()), document(), SLOT(redo()));
    connect(document(), SIGNAL(changed()), this, SLOT(updateActions()));
    document()->setStatus(FlowChart::Selectable);
    connect(saScheme, SIGNAL(mouseDown()), document(), SLOT(deselectAll()));

    FlowChartStyle st;
    QPalette pal = palette();
    st.setLineWidth(2);
    st.setNormalBackground(pal.color(QPalette::Base));
    st.setNormalForeground(pal.color(QPalette::WindowText));
    st.setNormalMarker(Qt::red);
    st.setSelectedBackground(pal.color(QPalette::Base));
    st.setSelectedForeground(Qt::darkGray);
    st.setNormalMarker(Qt::green);
    st.setFontSize(10);
    document()->setChartStyle(st);

    labelMenu = new QLabel(statusBar());

    statusBar()->setSizeGripEnabled(false);
    statusBar()->addWidget(labelMenu);
    labelMenu->setAlignment(Qt::AlignCenter);

    isSaved = true; //close application if no modification were made
    activeBlock = nullptr;
    connect(document(), SIGNAL(modified()), SLOT(slotDocumentChanged()));
    connect(this, SIGNAL(documentLoaded()), SLOT(slotDocumentLoaded()));
    connect(this, SIGNAL(documentSaved()), SLOT(slotDocumentSaved()));

    if (qApp->arguments().size() > 1) {
        QFile test(qApp->arguments().at(1));
        if(test.exists()) {
            slotOpenDocument(qApp->arguments().at(1));
        }
        else {
            QMessageBox::critical(this, tr("Failed to open a file"), tr("Unable to open file '%1'.").arg(qApp->arguments().at(1)));
        }
    }
}


void MainWindow::writeSettings()
{
    QSettings settings("algo", "application");

    settings.setValue("geometry", geometry());
    settings.setValue("windowState", saveState());
}
void MainWindow::closeEvent(QCloseEvent *event)
{
    if (okToContinue()) {
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}

void MainWindow::readSettings()

{
    QSettings settings("algo", "application");

    setGeometry(settings.value("geometry", QRect(100, 100, 800, 600)).toRect());
    restoreState(settings.value("windowState").toByteArray());

}
void MainWindow::setupUi()
{
    QApplication::setWindowIcon(QIcon(":/images/icon.png"));
    createActions();
    createMenu();
    createToolBar();
    QWidget *body = new QWidget;
    QWidget *zoomPanel = new QWidget;
    zoomPanel->setMinimumHeight(18);
    saScheme = new AlgScrollArea();
    QPalette pal = saScheme->palette();
    pal.setColor(QPalette::Window, pal.color(QPalette::Base));
    saScheme->setPalette(pal);

    setCentralWidget(body);
    QVBoxLayout *bodyLayout = new QVBoxLayout;
    bodyLayout->addWidget(saScheme);
    bodyLayout->addWidget(zoomPanel);
    body->setLayout(bodyLayout);
    zoomSlider = new QSlider(Qt::Horizontal, zoomPanel);
    zoomLabel = new QLabel;
    zoomLabel->setVisible(false);
    zoomSlider->setVisible(false);
    QHBoxLayout *zoomLayout= new QHBoxLayout;
    zoomLayout->addStretch();
    zoomLayout->addWidget(zoomLabel);
    zoomLayout->addWidget(zoomSlider);
    zoomPanel->setLayout(zoomLayout);
    zoomSlider->setRange(1, 20);
    zoomSlider->setSingleStep(1);
    zoomSlider->setPageStep(10);
    connect(zoomSlider, SIGNAL(valueChanged(int)),this, SLOT(setZoom(int)));
    zoomSlider->setValue(4);
    connect(saScheme, SIGNAL(zoomStepped(int)), this, SLOT(shiftZoom(int)));
    connect(saScheme, SIGNAL(scrollStepped(int)), this, SLOT(shiftScrollY(int)));

    createToolbox();

    helpWindow = new HelpWindow();
    helpWindow->setObjectName("help_window");
    helpWindow->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::RightDockWidgetArea, helpWindow);

    helpWindow->hide();

    dockVariable = new QDockWidget(this);
    dockVariable->setObjectName("dock_variable");
    dockVariable->setAllowedAreas(Qt::AllDockWidgetAreas);
    dockVariable->setMinimumWidth(300);
    addDockWidget(Qt::RightDockWidgetArea, dockVariable);
    variableTable = new QTableWidget(this);
    variableTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    variableTable->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    disconnect(variableTable->horizontalHeader(), SIGNAL(sectionPressed(int)),variableTable, SLOT(selectColumn(int)));

    connect(variableTable, SIGNAL(itemDoubleClicked(QTableWidgetItem *)), this, SLOT(slotEditVariable(QTableWidgetItem *)));
    variableTable->setColumnCount(3);
    variableTable->setRowCount(0);
    variableTable->setHorizontalHeaderLabels({"Имя","Значение", "Тип"});
    QFrame *fWidget = new QFrame(this);
    tBar = new QToolBar;
    tBar->setObjectName("var_toolbar");
    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(tBar);
    vl->addWidget(variableTable);
    fWidget->setLayout(vl);
    dockVariable->setWidget(fWidget);
    tBar->addAction(QIcon(":/images/add.png"), "Добавить", this, SLOT(slotAddVariable()));
    tBar->addAction(QIcon(":/images/remove.png"), tr("Удалить"), this, SLOT(slotDeleteVariable()));
    actVariables->setCheckable(true);
    actVariables->setChecked(dockVariable->isVisible());
    connect(actVariables, SIGNAL(triggered(bool)), dockVariable, SLOT(setVisible(bool)));
    connect(dockVariable, SIGNAL(visibilityChanged(bool)), actVariables, SLOT(setChecked(bool)));
    updateVariableTable();
    //dockVariable->hide();

    actHelp->setCheckable(true);
    actHelp->setChecked(helpWindow->isVisible());

    connect(actHelp, SIGNAL(triggered(bool)), helpWindow, SLOT(setVisible(bool)));
    connect(helpWindow, SIGNAL(visibilityChanged(bool)), actHelp, SLOT(setChecked(bool)));
}

QToolButton * createToolButton(const QString & fileName)
{
    QToolButton *Result = new QToolButton;
    Result->setIconSize(QSize(32, 32));
    Result->setIcon(QIcon(fileName));
    Result->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    Result->setAutoRaise(true);
    Result->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    Result->setCheckable(true);
    Result->setAutoExclusive(true);
    return Result;
}

void MainWindow::createToolbox()
{
    dockTools = new QDockWidget(this);
    dockTools->setObjectName("dock_tools");
    dockTools->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dockTools->setMinimumWidth(150);
    addDockWidget(Qt::LeftDockWidgetArea, dockTools);
    connect(dockTools, SIGNAL(visibilityChanged(bool)), this, SLOT(docToolsVisibilityChanged(bool)));

    tbArrow = createToolButton(":/images/arrow.png");
    tbArrow->setChecked(true);
    tbProcess = createToolButton(":/images/simple.png");
    tbIf = createToolButton(":/images/if.png");
    tbIo = createToolButton(":/images/io.png");
    tbOu = createToolButton(":/images/ou.png");

    connect(tbArrow, SIGNAL(pressed()), this, SLOT(slotToolArrow()));
    connect(tbProcess, SIGNAL(pressed()), this, SLOT(slotToolProcess()));
    connect(tbIf, SIGNAL(pressed()), this, SLOT(slotToolIf()));
    connect(tbIo, SIGNAL(pressed()), this, SLOT(slotToolIo()));
    connect(tbOu, SIGNAL(pressed()), this, SLOT(slotToolOu()));

    toolsWidget = new QFrame;
    toolsWidget->setFrameStyle(QFrame::StyledPanel | QFrame::Plain);
    QVBoxLayout *tl = new QVBoxLayout;
    tl->setSpacing(2);
    tl->addWidget(tbArrow);
    tl->addWidget(tbIo);
    tl->addWidget(tbOu);
    tl->addWidget(tbProcess);
    tl->addWidget(tbIf);
    tl->addStretch();
    toolsWidget->setLayout(tl);
    dockTools->setWidget(toolsWidget);
    actTools->setCheckable(true);
    actTools->setChecked(dockTools->isVisible());
    connect(actTools, SIGNAL(triggered(bool)), dockTools, SLOT(setVisible(bool)));
    connect(dockTools, SIGNAL(visibilityChanged(bool)), actTools, SLOT(setChecked(bool)));


}

void MainWindow::setNames()
{
    dockTools->setWindowTitle(tr("Панель инструментов"));
    tbArrow->setText(tr("Курсор"));
    tbProcess->setText(tr("Процесс"));
    tbIf->setText(tr("Условие"));
    tbIo->setText(tr("Ввод"));
    tbOu->setText(tr("Вывод"));
    actExit->setText(tr("Выход"));
    actExit->setStatusTip(tr("Exit from program"));
    actOpen->setText(tr("&Открыть..."));
    actOpen->setStatusTip(tr("Открыть файл"));
    actSave->setText(tr("&Сохранить"));
    actSave->setStatusTip(tr("Сохранить изменения"));
    actSaveAs->setText(tr("Сохранить &как..."));
    actSaveAs->setStatusTip(tr("Сохранить изменения в новом файле"));
    actExport->setText(tr("&Экспорт в PNG..."));
    actExport->setStatusTip(tr("Сохранить в виде растрового изображения"));
    actExportSVG->setText(tr("&Экспорт в SVG..."));
    actExportSVG->setStatusTip(tr("Сохранить в виде векторного ихображения"));
    actPrint->setText(tr("&Печать..."));
    actNew->setText(tr("&Создать..."));
    actNew->setStatusTip(tr("Создать новый файл"));
    actUndo->setText(tr("&Отменить"));
    actUndo->setStatusTip(tr("Отменить последнюю операцию"));
    actRedo->setText(tr("&Повторить"));
    actRedo->setStatusTip(tr("Повторить последнюю операцию"));
//    actCut->setText(tr("&Вырезать"));
//    actCopy->setText(tr("&Копировать"));
//    actPaste->setText(tr("Вст&авить"));
    actRun->setText(tr("Запустить"));
    actRun->setStatusTip(tr("Запустить алгоритм"));
    actStepByStep->setText(tr("Запустить пошагово"));
    actStepByStep->setStatusTip(tr("Запустить пошагово"));
    actStep->setText(tr("Сделать шаг"));
    actStep->setStatusTip(tr("Перейти на следующую итерацию алгоритма"));
    actStop->setText(tr("Остановить"));
    actStop->setStatusTip(tr("Остановить выполнение алгоритма"));
    actDelete->setText(tr("&Удалить"));
    actTest->setText(tr("&Тест"));
    actTest->setStatusTip(tr("Запуск тестирования"));
    actHelp->setText(tr("&Помощь"));
    actHelp->setStatusTip(tr("Показать окно помощи"));
    actAbout->setText(tr("&О программе"));
    actAboutQt->setText(tr("About &Qt"));
    actAboutQt->setStatusTip(tr("Информация о Qt"));
    actTools->setText(tr("&Панель инструментов"));
    actTools->setStatusTip(tr("Изменить видимость панели инструментов"));
    actVariables->setText(tr("&Переменные"));
    actVariables->setStatusTip(tr("Изменить видимость панели переменных"));



    actExit->setShortcut(tr("Alt+X"));
    actOpen->setShortcut(tr("Ctrl+O"));
    actSave->setShortcut(tr("Ctrl+S"));
    actTest->setShortcut(tr("Ctrl+T"));
    actNew->setShortcut(tr("Ctrl+N"));
    actUndo->setShortcut(tr("Ctrl+Z"));
    actRedo->setShortcut(tr("Ctrl+Y"));
//    actCut->setShortcut(tr("Ctrl+X"));
//    actCopy->setShortcut(tr("Ctrl+C"));
//    actPaste->setShortcut(tr("Ctrl+V"));
    actDelete->setShortcut(tr("Del"));
    actHelp->setShortcut(tr("F1"));
    actRun->setShortcut(tr("F5"));
    actStepByStep->setShortcut(tr("F6"));
    actStep->setShortcut(tr("F10"));
    actStop->setShortcut(tr("Shift+F5"));
    actPrint->setShortcut(tr("Ctrl+P"));
    actVariables->setShortcut(tr("F3"));
    actTools->setShortcut(tr("F2"));

    menuFile->setTitle(tr("&Файл"));
    menuEdit->setTitle(tr("&Правка"));
    menuHelp->setTitle(tr("&Справка"));
    menuWindow->setTitle(tr("&Окно"));
    menuStart->setTitle(tr("&Запуск"));

    toolBar->setWindowTitle(tr("Standard"));
    dockVariable->setWindowTitle(tr("Переменные"));


    if (!fileName.isEmpty())
        setWindowTitle(tr("%1 - Algo").arg(fileName));
    else
        setWindowTitle(tr("Algo"));
    helpWindow->setWindowTitle(tr("Помощь"));
    helpWindow->textBrowser->setSearchPaths(QStringList() << "help/"+QLocale().name());

    helpWindow->textBrowser->reload();


}

void MainWindow::createMenu()
{
    menuFile = menuBar()->addMenu("");
    menuFile->addAction(actNew);
    menuFile->addAction(actOpen);
    menuFile->addSeparator();
    menuFile->addAction(actSave);
    menuFile->addAction(actSaveAs);
    menuFile->addSeparator();
    menuFile->addAction(actExport);
    menuFile->addAction(actExportSVG);
    menuFile->addSeparator();
    menuFile->addAction(actPrint);
    menuFile->addSeparator();
    menuFile->addSeparator();
    menuFile->addAction(actExit);
    actAbout->isChecked();

    menuEdit = menuBar()->addMenu("");
    menuEdit->addAction(actUndo);
    menuEdit->addAction(actRedo);
    menuEdit->addSeparator();
//    menuEdit->addAction(actCut);
//    menuEdit->addAction(actCopy);
//    menuEdit->addAction(actPaste);
//    menuEdit->addSeparator();
    menuEdit->addAction(actDelete);

    menuWindow = menuBar()->addMenu("");
    menuWindow->addAction(actTools);
    menuWindow->addAction(actVariables);
    menuWindow->addSeparator();

    menuStart = menuBar()->addMenu("");
    menuStart->addAction(actTest);
    menuStart->addSeparator();
    menuStart->addAction(actRun);
    menuStart->addAction(actStepByStep);
    menuStart->addAction(actStep);
    menuStart->addAction(actStop);

    menuHelp = menuBar()->addMenu("");
    menuHelp->addAction(actHelp);
    menuHelp->addSeparator();
    menuHelp->addAction(actAbout);
    menuHelp->addAction(actAboutQt);
}

void MainWindow::createToolBar()
{
    toolBar = addToolBar("");
    toolBar->setObjectName("standard_toolbar");
    toolBar->setIconSize(QSize(20,20));
    toolBar->setToolButtonStyle(Qt::ToolButtonFollowStyle);

    toolBar->addAction(actNew);
    toolBar->addAction(actOpen);
    toolBar->addAction(actSave);
    toolBar->addSeparator();
    toolBar->addAction(actUndo);
    toolBar->addAction(actRedo);
    toolBar->addSeparator();
//    toolBar->addAction(actCut);
//    toolBar->addAction(actCopy);
//    toolBar->addAction(actPaste);
    toolBar->addSeparator();
    toolBar->addAction(actRun);
    toolBar->addAction(actStepByStep);
    toolBar->addSeparator();
    toolBar->addAction(actStep);
    toolBar->addAction(actStop);
    toolBar->addSeparator();
    toolBar->addAction(actTest);
    toolBar->addSeparator();
    toolBar->addAction(actTools);
    toolBar->addAction(actVariables);
    toolBar->addSeparator();
    toolBar->addAction(actHelp);
}


void MainWindow::docToolsVisibilityChanged(bool visible)
{
    actTools->setChecked(visible);
}

void MainWindow::createActions()
{
    actExit = new QAction(QIcon(":/images/exit.png"), "", this);
    actOpen = new QAction(QIcon(":/images/open_document_32_h.png"), "", this);
    actNew = new QAction(QIcon(":/images/new_document_32_h.png"), "", this);
    actSave = new QAction(QIcon(":/images/save_32_h.png"), "", this);
    actSaveAs = new QAction(this);
    actUndo = new QAction(QIcon(":/images/restart-3.png"), "", this);
    actRedo = new QAction(QIcon(":/images/restart-4.png"), "", this);
//    actCut = new QAction(QIcon(":/images/cut_clipboard_32_h.png"), "", this);
//    actCopy = new QAction(QIcon(":/images/copy_clipboard_32_h.png"), "", this);
//    actPaste = new QAction(QIcon(":/images/paste_clipboard_32_h.png"), "", this);
    actTest = new QAction(QIcon(":/images/test.png"), "", this);
    actDelete = new QAction(QIcon(":/images/delete_x_32_h.png"), "", this);
    actExport = new QAction(this);
    actExportSVG = new QAction(this);
    actHelp = new QAction(QIcon(":/images/help-icon.png"), "", this);
    actRun = new QAction(QIcon(":/images/run.png"), "", this);
    actStepByStep = new QAction(QIcon(":/images/runstep.png"), "", this);
    actStep = new QAction(QIcon(":/images/step.png"), "", this);
    actStop = new QAction(QIcon(":/images/stop.png"), "", this);
    actAbout = new QAction(this);
    actAboutQt = new QAction(this);
    actPrint = new QAction(QIcon(":/images/print_32_h.png"), "", this);
    actTools = new QAction(QIcon(":/images/toolbar.png"), "", this);
    actVariables = new QAction(QIcon(":/images/source-code.png"), "", this);


    connect(actExit, SIGNAL(triggered()), this, SLOT(close()));

    connect(actRun, SIGNAL(triggered()), this, SLOT(slotRun()));
    connect(actStepByStep, SIGNAL(triggered()), this, SLOT(slotStepByStep()));
    connect(actStep, SIGNAL(triggered()), this, SLOT(slotStep()));
    connect(actStop, SIGNAL(triggered()), this, SLOT(slotStop()));
    connect(actNew, SIGNAL(triggered()), this, SLOT(slotFileNew()));
    connect(actOpen, SIGNAL(triggered()), this, SLOT(slotFileOpen()));
    connect(actSave, SIGNAL(triggered()), this, SLOT(slotFileSave()));
    connect(actSaveAs, SIGNAL(triggered()), this, SLOT(slotFileSaveAs()));
    connect(actExport, SIGNAL(triggered()), this, SLOT(slotFileExport()));
    connect(actExportSVG, SIGNAL(triggered()), this, SLOT(slotFileExportSVG()));
    connect(actPrint, SIGNAL(triggered()), this, SLOT(slotFilePrint()));
//    connect(actCut, SIGNAL(triggered()), this, SLOT(slotEditCut()));
//    connect(actCopy, SIGNAL(triggered()), this, SLOT(slotEditCopy()));
//    connect(actPaste, SIGNAL(triggered()), this, SLOT(slotEditPaste()));
    connect(actTest, SIGNAL(triggered()), this, SLOT(slotRunTest()));
    connect(actDelete, SIGNAL(triggered()), this, SLOT(slotEditDelete()));

    connect(actAbout, SIGNAL(triggered()), this, SLOT(slotHelpAbout()));
    connect(actAboutQt, SIGNAL(triggered()), this, SLOT(slotHelpAboutQt()));


}


MainWindow::~MainWindow()
{
}

bool MainWindow::okToContinue()
{
    if (!isSaved) {
        QMessageBox messageBox(QMessageBox::Warning,
                               tr("Выход"), tr("Присутствуют несохраненные изменения. Вы уверены, что хотите закрыть программу?"),
                               QMessageBox::Yes | QMessageBox::No,
                               this);
        messageBox.setButtonText(QMessageBox::Yes, tr("Да"));
        messageBox.setButtonText(QMessageBox::No, tr("Нет"));

        if (messageBox.exec() != QMessageBox::Yes) {
            return false;
        }
    }
    return true;
}

void MainWindow::slotFileOpen()
{
    QString fn = QFileDialog::getOpenFileName ( this,
                                                tr("Select a file to open"), "", tr("Algorithm flowcharts (*.fcc)"));
    if(!fn.isEmpty())
    {

        if (!isSaved)
            if(QMessageBox::warning(this, tr("Присутствуют несохраненные изменения."), tr("Вы собираетесь открыть другой файл. Это отменит все несохраненные изменения в документе."),
                                    QMessageBox::Ok | QMessageBox::Cancel) != QMessageBox::Ok)
                return;
        slotOpenDocument(fn);

    }
}

void MainWindow::slotOpenDocument(const QString &fn) {
    emit documentUnloaded();
    fileName = fn;
    setWindowTitle(tr("%1 - Algorithm Flowchart Editor").arg(fileName));
    QFile xml(fileName);
    if (xml.exists())
    {
        xml.open(QIODevice::ReadOnly | QIODevice::Text);
        QDomDocument doc;
        if (doc.setContent(&xml, false))
        {
            document()->root()->setXmlNode(doc.firstChildElement().firstChildElement("terminal"));
            document()->root()->setNodesNext();
            document()->XMLToVars(doc.firstChildElement().lastChildElement("variables"));
            document()->setZoom(1);
        }
    }
    emit documentLoaded();
    updateVariableTable();
}

void MainWindow::slotFilePrint()
{
    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog pd(&printer, this);
    if (pd.exec() == QDialog::Accepted)
    {
        double oldZoom = document()->zoom();
        document()->setZoom(1);
        document()->setStatus(FlowChart::Display);
        Block *r = document()->root();
        r->graph->adjustSize(1);
        r->graph->adjustPosition(0,0);
        QRect page = printer.pageRect();
        double z =  page.width()  / (double) r->graph->width;
        if (r->graph->height * z > page.height())
        {
            z = page.height() / (double) r->graph->height;
        }
        if (z > (printer.resolution()/96.0)) z = printer.resolution()/96.0;
        document()->setZoom(z);
        r->graph->adjustSize(z);
        r->graph->adjustPosition(0, 0);
        QPainter canvas;
        canvas.begin(&printer);
        document()->paintTo(&canvas);
        canvas.end();
        document()->setZoom(oldZoom);
        document()->setStatus(FlowChart::Selectable);
    }
}

void MainWindow::slotFileNew()
{
    QProcess::startDetached(QApplication::applicationFilePath());
}

void MainWindow::slotStep()
{
    if (!document()->fActiveBlock)
    {
        document()->isRunning = false;
        document()->setEnabled(true);
        saScheme->setEnabled(true);
        dockTools->setEnabled(true);
        //dockVariable->setEnabled(true);
        updateActions();
        return;
    }
    try {
        document()->fActiveBlock = document()->fActiveBlock->execute();
    } catch (int code) {
        if (code) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Ошибка");
            switch (code) {
            case 0x01:
            {
                msgBox.setText("В блоке отсутствует переменная.");
                break;
            }
            case 0x02:
            {
                msgBox.setText("Перменная является числом, но первое слагаемое - строка.");
                break;
            }
            case 0x03:
            {
                msgBox.setText("Перменная является числом, но второе слагаемое - строка.");
                break;
            }
            case 0x04:
            {
                msgBox.setText("Некорректный ввод в блоке условия.");
                break;
            }
            case 0x05:
            {
                msgBox.setText("Типы данных в блоке условия не совпадают.");
                break;
            }
            default:
            {
                msgBox.setText("Ошибка.");
                break;
            }
            }
            msgBox.exec();
            document()->fActiveBlock = nullptr;
        }
    }
    document()->repaint();
    updateVariableTable();
    if (!document()->fActiveBlock)
    {
        document()->isRunning = false;
        document()->setEnabled(true);
        saScheme->setEnabled(true);
        dockTools->setEnabled(true);
        //dockVariable->setEnabled(true);
        updateActions();
        return;
    }
}

void MainWindow::slotStop()
{
    document()->fActiveBlock = nullptr;
    document()->isRunning = false;
    document()->setEnabled(true);
    dockTools->setEnabled(true);
    saScheme->setEnabled(true);
    //dockVariable->setEnabled(true);
    document()->repaint();
    updateActions();
}


void MainWindow::slotRun()
{
    if (console == nullptr)
        console = new ConsoleDialog();
    console->console->clear();
    if (document()->root()->item(0)->getItems().length() > 0)
        document()->fActiveBlock = document()->root()->item(0)->item(0);
    document()->setEnabled(false);
    dockTools->setEnabled(false);
    //dockVariable->setEnabled(false);
    saScheme->setEnabled(false);
    document()->isRunning = true;
    updateActions();
    while (document()->isRunning) {slotStep();}

}

void MainWindow::slotStepByStep()
{  
    if (console == nullptr)
        console = new ConsoleDialog();
    console->console->clear();
    if (document()->root()->item(0)->getItems().length() > 0)
        document()->fActiveBlock = document()->root()->item(0)->item(0);
    document()->setEnabled(false);
    dockTools->setEnabled(false);
    //dockVariable->setEnabled(false);
    saScheme->setEnabled(false);
    document()->isRunning = true;
    document()->repaint();
    updateActions();

}

void MainWindow::slotRunTest()
{
    QDialog choiceDlg;

    choiceDlg.setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    choiceDlg.setWindowTitle("Тест");
    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout *buttonLayout = new QHBoxLayout();
    QPushButton *btnOk = new QPushButton(tr("&OK"));
    QPushButton *btnCancel = new QPushButton(tr("&Отмена"));
    connect(btnOk, SIGNAL(clicked()), &choiceDlg, SLOT(accept()));
    connect(btnCancel, SIGNAL(clicked()), &choiceDlg, SLOT(reject()));
    buttonLayout->addStretch();
    buttonLayout->addWidget(btnOk);
    buttonLayout->addWidget(btnCancel);
    choiceDlg.setLayout(mainLayout);
    QComboBox *choice = new QComboBox();
    choice->addItem("Состояние переменной");
    choice->addItem("Вывод в консоль");
    choice->addItem("Случайный");
    QStandardItemModel *model = qobject_cast<QStandardItemModel *>(choice->model());
    Q_ASSERT(model != nullptr);
    bool out = true, var = true, rnd = true;
    if (variables.length() == 0)
    {
        var = false;
        out = false;
        rnd = false;
        btnOk->setEnabled(false);
    }
    if (!document()->isThereOutput(document()->root()))
    {
        out = false;
        rnd = false;
    }

    model->item(0)->setFlags(var ? model->item(0)->flags() | Qt::ItemIsEnabled
                            : model->item(0)->flags() & ~Qt::ItemIsEnabled);
    model->item(1)->setFlags(out ? model->item(1)->flags() | Qt::ItemIsEnabled
                            : model->item(1)->flags() & ~Qt::ItemIsEnabled);
    model->item(2)->setFlags(rnd ? model->item(2)->flags() | Qt::ItemIsEnabled
                            : model->item(2)->flags() & ~Qt::ItemIsEnabled);

    QLabel *label= new QLabel("Выберите тип вопроса:");

    mainLayout->addWidget(label);
    mainLayout->addWidget(choice);
    mainLayout->addLayout(buttonLayout);
    if (choiceDlg.exec() == QDialog::Accepted)
    {
        int index = choice->currentIndex();
        if (index == 2)
        {
            index = rand() % 2;
        }
        if (index == 0){

            QDialog dlg;
            dlg.setWindowTitle("Тест");
            dlg.setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
            QVBoxLayout *mainLayoutVar = new QVBoxLayout();
            QHBoxLayout *buttonLayoutVar = new QHBoxLayout();
            QPushButton *btnOkVar = new QPushButton(tr("&OK"));
            QPushButton *btnCancelVar = new QPushButton(tr("&Отмена"));
            connect(btnOkVar, SIGNAL(clicked()), &dlg, SLOT(accept()));
            connect(btnCancelVar, SIGNAL(clicked()), &dlg, SLOT(reject()));
            buttonLayoutVar->addStretch();
            buttonLayoutVar->addWidget(btnOkVar);
            buttonLayoutVar->addWidget(btnCancelVar);
            dlg.setLayout(mainLayoutVar);
            bool toRun = true;
            QLineEdit *answer;
            QLabel *question;
            int i = 0;
            if (variables.length() != 0){
                i = qrand() % variables.length();
                QString varName = variables.at(i)->getName();
                answer = new QLineEdit();
                question = new QLabel("Чему будет равна переменная " + varName + " в конце выполнения алгоритма?");
                mainLayoutVar->addWidget(question);
                mainLayoutVar->addWidget(answer);
            }
            else{
                question = new QLabel("Запуск теста невозможен т.к. отсутствуют переменные.");
                mainLayoutVar->addWidget(question);
                toRun = false;
            }
            mainLayoutVar->addLayout(buttonLayoutVar);

            if (dlg.exec() == QDialog::Accepted && toRun)
            {
                slotRun();
                if (variables.at(i)->getValue() == answer->text())
                {
                    QMessageBox messageBox;
                    messageBox.information(nullptr,"Верно!","Вы ответили правильно.");
                    messageBox.setFixedSize(500,200);
                }
                else {
                    QMessageBox messageBox;
                    messageBox.critical(nullptr,"Неверно!","Вы ответили неверно. Попробуйте ещё раз.");
                    messageBox.setFixedSize(500,200);
                }
            }
        }
        else if (index == 1){

            QDialog dlg;
            dlg.setWindowTitle("Тест");
            dlg.setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
            QVBoxLayout *mainLayoutVar = new QVBoxLayout();
            QHBoxLayout *buttonLayoutVar = new QHBoxLayout();
            QPushButton *btnOkVar = new QPushButton(tr("&OK"));
            QPushButton *btnCancelVar = new QPushButton(tr("&Отмена"));
            connect(btnOkVar, SIGNAL(clicked()), &dlg, SLOT(accept()));
            connect(btnCancelVar, SIGNAL(clicked()), &dlg, SLOT(reject()));
            buttonLayoutVar->addStretch();
            buttonLayoutVar->addWidget(btnOkVar);
            buttonLayoutVar->addWidget(btnCancelVar);
            dlg.setLayout(mainLayoutVar);
            bool toRun = true;
            QLineEdit *answer;
            QLabel *question;
            int i = 0;
            if (variables.length() != 0){
                if (document()->isThereOutput(document()->root())){
                    answer = new QLineEdit();
                    question = new QLabel("Что выведет программа в окно консоли?");
                    mainLayoutVar->addWidget(question);
                    mainLayoutVar->addWidget(answer);
                }
                else {
                    question = new QLabel("Запуск теста невозможен т.к. нет блоков вывода.");
                    mainLayoutVar->addWidget(question);
                    toRun = false;
                }
            }
            else{
                question = new QLabel("Запуск теста невозможен т.к. отсутствуют переменные.");
                mainLayoutVar->addWidget(question);
                toRun = false;
            }
            mainLayoutVar->addLayout(buttonLayoutVar);

            if (dlg.exec() == QDialog::Accepted && toRun)
            {
                slotRun();
                QString consoleText = console->console->toPlainText();
                QStringList lst = consoleText.split('\n');
                for (int i = 0; i < lst.length(); i++){
                    if (lst.at(i).contains("input>")) lst.removeAt(i);
                }
                consoleText = lst.join("");
                if (consoleText.simplified().remove("output>").replace(" ", "") == answer->text().replace(" ", ""))
                {
                    QMessageBox messageBox;
                    messageBox.information(nullptr,"Верно!","Вы ответили правильно.");
                    messageBox.setFixedSize(500,200);
                }
                else {
                    QMessageBox messageBox;
                    messageBox.critical(nullptr,"Неверно!","Вы ответили неверно. Попробуйте ещё раз.");
                    messageBox.setFixedSize(500,200);
                }
            }
        }
    }

}

void MainWindow::slotFileSave()
{
    if (fileName.isEmpty())
    {
        slotFileSaveAs();
    }
    else
    {
        QDomDocument doc = document()->document();
        QString xmlString = doc.toString(2);
        QFile xml(fileName);
        xml.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate);
        QTextStream stream(&xml);
        stream.setCodec(QTextCodec::codecForName("utf-8"));
        stream << xmlString;
        xml.close();
        emit documentSaved();
    }
}

void MainWindow::slotFileSaveAs()
{
    QString fn = QFileDialog::getSaveFileName(this, tr("Select a file to save"), "", tr("Algorithm flowcharts (*.fcc)"));
    if (!fn.isEmpty())
    {
        if(fn.right(4).toLower() != ".fcc") fn += ".fcc";
        fileName = fn;
        setWindowTitle(tr("%1 - Algorithm Flowchart Editor").arg(fileName));
        slotFileSave();
    }
}


void MainWindow::updateVariableTable() {
    variableTable->setRowCount(0);
    if (document())
    {
        for (int i = 0; i < variables.length(); i++){

            variableTable->insertRow(variableTable->rowCount());
            QTableWidgetItem *nameItem = new QTableWidgetItem(variables.at(i)->getName());
            variableTable->setItem(variableTable->rowCount()-1, 0, nameItem);
            QTableWidgetItem *valueItem = new QTableWidgetItem(variables.at(i)->getValue());
            variableTable->setItem(variableTable->rowCount()-1, 1, valueItem);
            QTableWidgetItem *typeItem = new QTableWidgetItem(variables.at(i)->getType());
            typeItem->setFlags(typeItem->flags() ^ Qt::ItemIsEditable);
            nameItem->setFlags(nameItem->flags() ^ Qt::ItemIsEditable);
            valueItem->setFlags(valueItem->flags() ^ Qt::ItemIsEditable);
            variableTable->setItem(variableTable->rowCount()-1, 2, typeItem);
        }
    }
}


void MainWindow::slotEditVariable(QTableWidgetItem *item) {
    QDialog dlg;
    dlg.setWindowTitle("Изменение переменной");
    dlg.setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QVBoxLayout *mainLayoutVar = new QVBoxLayout();
    QHBoxLayout *buttonLayoutVar = new QHBoxLayout();
    QPushButton *btnOkVar = new QPushButton(tr("&OK"));
    QPushButton *btnCancelVar = new QPushButton(tr("&Отмена"));
    connect(btnOkVar, SIGNAL(clicked()), &dlg, SLOT(accept()));
    connect(btnCancelVar, SIGNAL(clicked()), &dlg, SLOT(reject()));
    buttonLayoutVar->addStretch();
    buttonLayoutVar->addWidget(btnOkVar);
    buttonLayoutVar->addWidget(btnCancelVar);
    dlg.setLayout(mainLayoutVar);
    QLineEdit *newValue;
    QLabel *lbl;

    newValue = new QLineEdit();
    lbl = new QLabel("Введите новое значение переменной.");
    mainLayoutVar->addWidget(lbl);
    mainLayoutVar->addWidget(newValue);

    mainLayoutVar->addLayout(buttonLayoutVar);

    int row = item->row();
    if (dlg.exec() == QDialog::Accepted)
    {
        if (variables.at(row) != nullptr)
            if (newValue->text() != nullptr)
                variables.at(row)->setValue(newValue->text());
    }
    updateVariableTable();
}

void MainWindow::slotDeleteVariable() {
    if (variables.length() == 0)
    {
        QMessageBox messageBox(QMessageBox::Warning,
                               tr("Удаление переменной"), tr("Отсутствуют переменные для удаления."),
                               QMessageBox::Ok, this);
        if (messageBox.exec() == QMessageBox::Ok) {
            return;
        }
    }
    else if (variableTable->selectedItems().length() == 0) {
        QMessageBox messageBox(QMessageBox::Warning,
                               tr("Удаление переменной"), tr("Вы не выбрали переменную."),
                               QMessageBox::Ok, this);
        if (messageBox.exec() == QMessageBox::Ok) {
            return;
        }
    }
    else {
        QMessageBox messageBox(QMessageBox::Warning,
                               tr("Удаление переменной"), tr("Вы уверены, что хотите удалить переменную?"),
                               QMessageBox::Yes | QMessageBox::No,
                               this);
        messageBox.setButtonText(QMessageBox::Yes, tr("Да"));
        messageBox.setButtonText(QMessageBox::No, tr("Нет"));

        if (messageBox.exec() != QMessageBox::Yes) {
            return;
        }
        else
            variables.removeAt(variableTable->selectedItems().first()->row());

    }

    updateVariableTable();
}

void MainWindow::slotAddVariable() {
    QDialog dlg;
    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout *buttonLayout = new QHBoxLayout();
    QPushButton *btnOk = new QPushButton(tr("&OK"));
    QPushButton *btnCancel = new QPushButton(tr("&Отмена"));
    connect(btnOk, SIGNAL(clicked()), &dlg, SLOT(accept()));
    connect(btnCancel, SIGNAL(clicked()), &dlg, SLOT(reject()));
    buttonLayout->addStretch();
    buttonLayout->addWidget(btnOk);
    buttonLayout->addWidget(btnCancel);
    dlg.setLayout(mainLayout);
    QLineEdit *name = new QLineEdit();
    QLabel *nameLabel = new QLabel(tr("&Название:"));

    QComboBox *type = new QComboBox();
    type->insertItem(0, "String");
    type->insertItem(1, "Double");
    QLabel *typeLabel = new QLabel(tr("&Тип:"));

    QLineEdit *value = new QLineEdit();

    QLabel *valueLabel = new QLabel(tr("&Значение:"));
    dlg.setWindowTitle(tr("Добавление переменной"));
    nameLabel->setBuddy(name);
    typeLabel->setBuddy(type);
    valueLabel->setBuddy(value);

    QHBoxLayout *box = new QHBoxLayout;
    box->addWidget(nameLabel);
    box->addWidget(name);
    box->addWidget(typeLabel);
    box->addWidget(type);
    box->addWidget(valueLabel);
    box->addWidget(value);
    mainLayout->addLayout(box);
    mainLayout->addLayout(buttonLayout);

    if (dlg.exec() == QDialog::Accepted)
    {
        if(document() && name->text() != nullptr)
        {
            Variable *a = new Variable();

            a->setName(name->text());
            for (int i = 0; i < variables.length(); i++){
                if (variables.at(i)->getName() == a->getName()){
                    QMessageBox messageBox;
                    messageBox.critical(nullptr,"Ошибка","Переменная с таким именем уже существует!");
                    messageBox.setFixedSize(500,200);
                    return;
                }

            }
            a->setType(type->currentText());
            if (value->text() != nullptr)
                a->setValue(value->text());
            else {
                if (a->getType() == "String")
                    a->setValue("");
                else a->setValue("0");
            }
            variables.append(a);
            updateVariableTable();
        }
    }
}

void MainWindow::slotDocumentSaved() {
    isSaved = true;
}

void MainWindow::slotDocumentChanged() {
    isSaved = false;
}

void MainWindow::slotDocumentLoaded() {
    isSaved = true;
}


void MainWindow::slotFileExport()
{
    QString filter = getWriteFormatFilter();
    QString sf = getFilterFor("png");
    QString fn = QFileDialog::getSaveFileName(this, tr("Select a file to export"), "", filter, &sf);
    if(!fn.isEmpty())
    {
        qDebug() << "Selected filter: " << sf;
        QRegExp rx("\\(\\*([^\\)]+)\\)$");
        QStringList masks;
        if(rx.indexIn(sf)!= -1) {
            // each filter may content several masks splitted by a spaces
            masks = rx.cap(1).split(" ", QString::SkipEmptyParts);

            bool matches = false;
            for(int i = 0; i < masks.size(); ++i) {
                QString ex = masks.at(i).toLower();
                if(fn.toLower().endsWith(ex)) {
                    matches = true;
                    break;
                }
            }

            // if no extension or a wrong extension is added then the correct extension will be appended
            if(!matches && !masks.empty()) {
                fn += masks.first().toLower();
            }
        }
        double oldZoom = document()->zoom();
        document()->setZoom(1);
        document()->setStatus(FlowChart::Display);
        Block *r = document()->root();
        r->graph->adjustSize(1);
        r->graph->adjustPosition(0,0);
        QImage img(r->graph->width, r->graph->height, QImage::Format_ARGB32_Premultiplied);
        img.fill(0);
        QPainter canvas(&img);
        canvas.setRenderHint(QPainter::Antialiasing);
        document()->paintTo(&canvas);
        img.save(fn);
        document()->setZoom(oldZoom);
        document()->setStatus(FlowChart::Selectable);
    }
}

void MainWindow::slotFileExportSVG()
{
    QString filter = getWriteFormatFilter();
    QString fn = QFileDialog::getSaveFileName(this, tr("Select a file to export"), "", getFilterFor("svg"));
    if(!fn.isEmpty())
    {
        if(fn.right(4).toLower() != ".svg") fn += ".svg";
        double oldZoom = document()->zoom();
        document()->setZoom(1);
        document()->setStatus(FlowChart::Display);
        Block *r = document()->root();
        r->graph->adjustSize(1);
        r->graph->adjustPosition(0,0);
        QSvgGenerator svg;
        svg.setSize(QSize(r->graph->width, r->graph->height));
        svg.setResolution(90);
        svg.setFileName(fn);
        QPainter canvas(&svg);
        canvas.setRenderHint(QPainter::Antialiasing);
        document()->paint(&canvas, r);

        document()->setZoom(oldZoom);
        document()->setStatus(FlowChart::Selectable);
    }
}

void MainWindow::slotEditDelete()
{
    if(document())
    {
        if(document()->status() == FlowChart::Selectable)
        {
            document()->deleteActiveBlock();
        }
    }
}


void MainWindow::slotHelpAbout()
{
    QDialog dlg;
    QPushButton *ok = new QPushButton(tr("&OK"));
    QLabel *text = new QLabel(tr("<html><h1>Algo</h1><p>Algorithm Flowchart Editor. Версия %0</p> \
                                 <p>Copyright (C) 2009-2014 Victor Zinkevich.</p> \
                                 <p>Copyright (C) 2019 Anton Gruzdev. </p> \
                                 <p>Algo is free software: you can redistribute it and/or </p> \
                                 <p> modify it under the terms of the GNU General Public License as </p> \
                                 <p> published by the Free Software Foundation, either version 3 of the </p> \
                                 <p> License, or (at your option) any later version. </p> \
                                 <p>Программа выполнена в качестве дипломной работы.</p></html>").arg("1"));
                                 QLabel *ico = new QLabel();
                              ico->setPixmap(QPixmap(":/images/icon.png"));
            QGridLayout *layout = new QGridLayout;
    QHBoxLayout *bl = new QHBoxLayout;
    bl->addStretch();
    bl->addWidget(ok);
    layout->addWidget(ico, 0, 0, 2, 1, Qt::AlignTop);
    layout->addWidget(text, 0, 1);
    layout->addLayout(bl, 1, 1);
    dlg.setLayout(layout);
    connect(ok, SIGNAL(clicked()), &dlg, SLOT(accept()));
    dlg.exec();
}

void MainWindow::slotHelpAboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::slotStatusChanged()
{
    if(document())
    {
        if(document()->status() == FlowChart::Selectable)
        {
            tbArrow->setChecked(true);
        }
    }
}

void MainWindow::updateActions()
{
    if(document())
    {
        if (actTools->isVisible())
        {
            actTools->setEnabled(false);
        }
        actRun->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing && !document()->isRunning);
        actStepByStep->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing && !document()->isRunning);
        actStep->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing && document()->isRunning);
        actStop->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing && document()->isRunning);

        actVariables->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actTools->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actHelp->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actUndo->setEnabled(document()->canUndo() && document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actRedo->setEnabled(document()->canRedo() && document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actOpen->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actSave->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actSaveAs->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actPrint->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actExport->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
        actExportSVG->setEnabled(document()->status() != FlowChart::Insertion && document()->status() != FlowChart::Changing);
//        actCopy->setEnabled(document()->status() == FlowChart::Selectable && document()->activeBlock());
//        actCut->setEnabled(document()->status() == FlowChart::Selectable && document()->activeBlock());
//        actPaste->setEnabled(document()->status() == FlowChart::Selectable && document()->canPaste());
        actDelete->setEnabled(document()->status() == FlowChart::Selectable && document()->activeBlock());
    }
    else
    {
        actUndo->setEnabled(false);
        actRedo->setEnabled(false);
        actOpen->setEnabled(false);
        actSave->setEnabled(false);
        actSaveAs->setEnabled(false);
        actPrint->setEnabled(false);
        actExport->setEnabled(false);
        actExportSVG->setEnabled(false);
//        actCopy->setEnabled(false);
//        actCut->setEnabled(false);
//        actPaste->setEnabled(false);
        actDelete->setEnabled(false);
    }
}


void MainWindow::slotChangeLeft(){
    desicionDialog->reject();
    document()->setBuffer("true");
    document()->setStatus(FlowChart::Changing);

}

void MainWindow::slotChangeRight(){
    desicionDialog->reject();
    document()->setBuffer("false");
    document()->setStatus(FlowChart::Changing);
}

void MainWindow::slotEditBlock(Block *aBlock)
{
    if(aBlock)
    {
        QDialog dlg;
        QVBoxLayout *mainLayout = new QVBoxLayout();
        QHBoxLayout *buttonLayout = new QHBoxLayout();
        QPushButton *btnOk = new QPushButton(tr("&OK"));
        QPushButton *btnCancel = new QPushButton(tr("&Отмена"));
        connect(btnOk, SIGNAL(clicked()), &dlg, SLOT(accept()));
        connect(btnCancel, SIGNAL(clicked()), &dlg, SLOT(reject()));
        buttonLayout->addStretch();
        buttonLayout->addWidget(btnOk);
        buttonLayout->addWidget(btnCancel);
        dlg.setLayout(mainLayout);
        if( aBlock->getAttribute("type") == "if")
        {
            desicionDialog = new QDialog();
            connect(btnOk, SIGNAL(clicked()), desicionDialog, SLOT(accept()));
            connect(btnCancel, SIGNAL(clicked()), desicionDialog, SLOT(reject()));
            desicionDialog->setLayout(mainLayout);
            QLineEdit *text = new QLineEdit();
            text->setMaxLength(30);
            QLabel *lab = new QLabel(tr("&Содержание:"));
            QPushButton *setTrue = new QPushButton("Изменить левую линию");
            QPushButton *setFalse = new QPushButton("Изменить правую линию");
            connect(setTrue, SIGNAL(clicked()), this, SLOT(slotChangeLeft()));
            connect(setFalse, SIGNAL(clicked()), this, SLOT(slotChangeRight()));
            QString attr = "cond";
            desicionDialog->setWindowTitle(tr("Условие"));
            lab->setText(tr("&Условие:"));
            text->setText(aBlock->getAttribute(attr));
            lab->setBuddy(text);
            QGridLayout *box = new QGridLayout;
            box->addWidget(lab, 0, 0);
            box->addWidget(text, 0, 1);
            box->addWidget(setTrue, 1, 0);
            box->addWidget(setFalse, 1, 1);
            box->addWidget(btnOk, 2, 0);
            box->addWidget(btnCancel, 2, 1);
            mainLayout->addLayout(box);
            //mainLayout->addLayout(buttonLayout);
            int res = desicionDialog->exec();
            if (res == QDialog::Accepted)
            {
                if(document())
                {
                    if(text->text().split(QRegExp("\\s+"), QString::SkipEmptyParts).length() != 3){
                        QMessageBox messageBox;
                        messageBox.critical(nullptr,"Ошибка","Некорректный ввод условия! Пример ввода: 'x > 0'.");
                        messageBox.setFixedSize(500,200);
                        return;
                    }
                    document()->makeUndo();
                    aBlock->setAttribute(attr, text->text());
                    document()->update();
                    document()->makeChanged();
                }
            }
        }

        else if (aBlock->getAttribute("type") == "process")
        {
            dlg.setWindowTitle(tr("Процесс"));

            QGridLayout *gl = new QGridLayout();
            QLineEdit *leFirst = new QLineEdit();
            QLineEdit *leSecond = new QLineEdit();
            QComboBox *cbSign = new QComboBox();
            cbSign->insertItem(0, "--");
            cbSign->insertItem(1, "+");
            cbSign->insertItem(2, "-");
            cbSign->insertItem(3, "*");
            cbSign->insertItem(4, "/");
            QLabel *labTo = new QLabel(tr("&Выражение:"));
            QComboBox *cbVar = new QComboBox();
            cbVar->insertItem(0, "--");
            for (int i = 0; i < variables.length(); i++){
                cbVar->insertItem(i + 1, variables.at(i)->getName());
            }
            QLabel *labVar = new QLabel(tr("&Переменная:"));
            QLabel *equal = new QLabel(tr("="));
            gl->addWidget(labVar, 0, 0);
            gl->addWidget(cbVar, 1, 0);
            gl->addWidget(equal, 1, 1);
            gl->addWidget(labTo, 0, 2);
            gl->addWidget(leFirst, 1, 2);
            gl->addWidget(cbSign, 1, 3);
            gl->addWidget(leSecond, 1, 4);
            labTo->setBuddy(leFirst);
            labVar->setBuddy(cbVar);

            mainLayout->addLayout(gl);
            mainLayout->addLayout(buttonLayout);
            int index = cbVar->findText(aBlock->getAttribute("variable"));
            if ( index != -1 ) { // -1 for not found
                cbVar->setCurrentIndex(index);
            }
            int index2 = cbSign->findText(aBlock->getAttribute("sign"));
            if ( index2 != -1 ) { // -1 for not found
                cbSign->setCurrentIndex(index2);
            }
            leFirst->setText(aBlock->getAttribute("first"));
            leSecond->setText(aBlock->getAttribute("second"));
            if (dlg.exec() == QDialog::Accepted)
            {
                if(document())
                {
                    if (cbVar->currentText() != "--" && leFirst->text() != ""){
                        document()->makeUndo();
                        if (cbSign->currentText() != "--") {
                            aBlock->setAttribute("variable", cbVar->currentText());
                            aBlock->setAttribute("first", leFirst->text());
                            aBlock->setAttribute("sign", cbSign->currentText());
                            aBlock->setAttribute("second", leSecond->text());
                        }
                        else {
                            aBlock->setAttribute("variable", cbVar->currentText());
                            aBlock->setAttribute("first", leFirst->text());
                            aBlock->setAttribute("sign", "");
                            aBlock->setAttribute("second", "");
                        }
                        document()->update();
                        document()->makeChanged();
                    }
                }
            }
        }
        else if(aBlock->getAttribute("type") == "io" || aBlock->getAttribute("type") == "ou")
        {
            if (aBlock->getAttribute("type") == "io")
            {
                dlg.setWindowTitle(tr("Ввод"));
            }
            else if (aBlock->getAttribute("type") == "ou")
            {
                dlg.setWindowTitle(tr("Вывод"));
            }
            QVBoxLayout *vl = new QVBoxLayout();
            QComboBox *var = new QComboBox();
            var->insertItem(0, "--");
            for (int i = 0; i < variables.length(); i++){
                var->insertItem(i+1, variables.at(i)->getName());
            }
            QLabel *label = new QLabel(tr("Выберите переменную из списка:"));
            vl->addWidget(label);
            vl->addWidget(var);
            mainLayout->addLayout(vl);
            mainLayout->addLayout(buttonLayout);
            int index = var->findText(aBlock->getAttribute("var"));
            if ( index != -1 ) { // -1 for not found
                var->setCurrentIndex(index);
            }
            if (dlg.exec() == QDialog::Accepted)
            {
                if(document())
                {
                    if (var->currentIndex() != 0){
                        document()->makeUndo();
                        aBlock->setAttribute("var", var->currentText());
                        document()->update();
                        document()->makeChanged();
                    }
                }
            }
        }

    }
}

void MainWindow::slotToolArrow()
{
    if(document())
    {
        document()->setStatus(FlowChart::Selectable);
        document()->update();
    }
}
void MainWindow::slotToolProcess()
{
    if(document())
    {
        document()->setBuffer("process");
        if(!document()->buffer().isEmpty())
        {
            document()->setStatus(FlowChart::Insertion);
            document()->setMultiInsert(false);
        }
    }
}

void MainWindow::slotToolIf()
{
    if(document())
    {
        document()->setBuffer("if");
        if(!document()->buffer().isEmpty())
        {
            document()->setStatus(FlowChart::Insertion);
            document()->setMultiInsert(false);
        }
    }
}

void MainWindow::slotToolIo()
{
    if(document())
    {
        document()->setBuffer("io");
        if(!document()->buffer().isEmpty())
        {
            document()->setStatus(FlowChart::Insertion);
            document()->setMultiInsert(false);
        }
    }

}


void MainWindow::slotToolOu()
{
    if(document())
    {
        document()->setBuffer("ou");
        if(!document()->buffer().isEmpty())
        {
            document()->setStatus(FlowChart::Insertion);
            document()->setMultiInsert(false);
        }
    }

}


QString MainWindow::getFilterFor(const QString & fileExt)
{
    return tr("%1 image (*.%2)").arg(fileExt.toUpper(), fileExt);
}

QString MainWindow::getWriteFormatFilter()
{
    QString result;
    QList<QByteArray> formats = QImageWriter::supportedImageFormats();
    for(int i = 0; i < formats.size(); ++i)
    {
        if(!result.isEmpty()) result.append(";;");
        result.append(getFilterFor(formats.at(i)));
    }
    return result;
}


void MainWindow::setDocument(FlowChart * aDocument)
{
    fDocument = aDocument;
    saScheme->setWidget(fDocument);
    saScheme->setAutoFillBackground(true);
    fDocument->show();
    fDocument->move(0,0);
}
void MainWindow::setZoom(int quarts)
{
    if(quarts > 20) quarts = 20;
    if(quarts < 1) quarts = 1;
    zoomLabel->setText(tr("Zoom: %1 %").arg(quarts * 25));
    if (document())
    {
        document()->setZoom(quarts * 25 / 100.0);
    }
}

void MainWindow::shiftZoom(int step)
{
    int z = zoomSlider->value();
    zoomSlider->setValue(z + step);
}

void MainWindow::shiftScrollY(int step)
{
    saScheme->viewport()->scroll(0, step);
}

