QT += widgets
QT += core gui
QT += xml
QT += printsupport
QT += svg

INCLUDEPATH += .

CONFIG += exceptions \
    rtti \
    stl
OBJECTS_DIR = build
UI_DIR = build
MOC_DIR = build
RCC_DIR = build
TARGET = algo

requires(qtConfig(fontcombobox))

win32:RC_ICONS += algo.ico
#macx:ICON = algo.icns


win32 {
    for(qm, QT_QM) {
        system(copy /Y $$replace(qm, /, \\) $$replace(_PRO_FILE_PWD_, /, \\)\\locale\\)
    }

}
!win32 {
    system(cp -f $$QT_QM locale/)
}
HEADERS	    =   mainwindow.h \
    globvars.h \
    helpwindow.h \
    flowchart.h \
    flowchartstyle.h \
    console.h
    #arrow.h
SOURCES	    =   main.cpp \
    mainwindow.cpp \
    helpwindow.cpp \
    flowchart.cpp \
    flowchartstyle.cpp \
    console.cpp
    #arrow.cpp
RESOURCES   =	diagramscene.qrc


# install
PREFIX = $$[QT_INSTALL_EXAMPLES]/widgets/graphicsview/diagramscene
target.path = $$PREFIX/bin/
INSTALLS += target
INSTALLS += pixmaps
icons.path = $$PREFIX/share/icons
icons.files = algo.ico
INSTALLS += icons
desktops.path = $$PREFIX/share/applications
desktops.files = algo.desktop
INSTALLS += desktops
helps.path = $$PREFIX/share/algo/help/
helps.files = help/*
INSTALLS += helps
generators.path = $$PREFIX/share/algo/generators
generators.files = generators/*
INSTALLS += generators
mime.path = $$PREFIX/share/mime/packages
mime.files = algo.xml
INSTALLS += mime
