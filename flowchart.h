/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#ifndef FLOWCHART_H
#define FLOWCHART_H

#include <QtCore>
#include <QtGui>
#include <QWidget>
#include <QGraphicsPixmapItem>
#include <QDomDocument>
#include <QMessageBox>
#include "flowchartstyle.h"
#include "console.h"
#include "globvars.h"

class Block;
class FlowChart;
class BlockGraphics;

inline ConsoleDialog *console;
static QList <Block *> blocks;
inline QList <Variable *> variables;


class InsertionPoint
{
private:
    QPointF fPoint;
    Block *fBranch;
    int fIndex;
public:
    InsertionPoint()
    {
        fPoint = QPoint();
        fBranch = nullptr;
        fIndex = -1;
    }
    QPointF point() const { return fPoint; }
    Block *branch() const { return fBranch; }
    int index() const { return fIndex; }
    void setPoint(const QPointF &aPoint) { fPoint = aPoint; }
    void setBranch(Block *aBranch) { fBranch = aBranch; }
    void setIndex(int aIndex) { fIndex = aIndex; }
    bool isNull() const { return fPoint.isNull() && fBranch == nullptr && fIndex == -1; }
};

Q_DECLARE_TYPEINFO(InsertionPoint, Q_MOVABLE_TYPE);

class IBlock
{
public:
    virtual ~IBlock(){}
    virtual Block *execute() = 0;
    virtual void setNext() = 0;

};

class Block : public IBlock
{
private:
    QHash<QString, QString> attributes;
    QList<Block *> items;
    Block *parent;
    static int lastID;
    int id;

public:
    Block();
    ~Block();
    BlockGraphics *graph;
    //Getters and setters
    QString getAttribute(const QString & name) const { return attributes.value(name, QString());}
    void setAttribute(QString name, QString value) { attributes[name] = value;}
    void setNodesNext();
    QList<Block *> getItems() const { return this->items;}
    void addItem(Block *aBlock) { this->items.append(aBlock);}
    QHash<QString, QString> getAttributes() const { return this->attributes;}
    Block *getParent() const { return this->parent;}
    void setParrent(Block *aBlock) { this->parent = aBlock;}
    int getId() const {return this->id;}
    int index();
    void insert(int newIndex, Block *aBlock);
    void remove(Block *aBlock);
    void append(Block *aBlock);
    void deleteObject(int aIndex);
    Block * item(int aIndex) const { return items.at(aIndex); }
    void setItem(int aIndex, Block *aBlock);
    void clear();
    Block * blockAt(int px, int py);
    QDomElement xmlNode(QDomDocument & doc) const;
    void setXmlNode(const QDomElement & node);
    void addSetup(QString type);
    void insertBlock(int aIndex, QString type);
};

class BlockGraphics
{
public:
    BlockGraphics();
    Block *block;
    double x, y, width, height;
    double topMargin, bottomMargin, leftMargin, rightMargin;
    void adjustSize(const double aZoom);
    void adjustPosition(const double ox, const double oy);

};

class Decision : public Block
{
public:
    virtual ~Decision() override{}
    virtual Block *execute() override;
    virtual void setNext () override;
    Block *getNextTrue() {return this->nextTrue;}
    Block *getNextFalse() {return this->nextFalse;}
    void setNextTrue(Block *aBlock) {this->nextTrue = aBlock;}
    void setNextFalse(Block *aBlock) {this->nextFalse = aBlock;}
private:
    Block *nextTrue;
    Block *nextFalse;

};

class Process : public Block
{
public:
    virtual ~Process() override {}
    virtual Block *execute() override;
    virtual void setNext () override;
    Block *getNext()  {return this->next;}
private:
    Block *next;


};

class Input : public Block
{
public:
    virtual ~Input() override{}
    virtual Block *execute() override;
    virtual void setNext () override;
    Block *getNext() {return this->next;}
private:
    Block *next;

};

class Output : public Block
{
public:
    virtual ~Output() override{}
    virtual Block *execute() override;
    virtual void setNext () override;
    Block *getNext() {return this->next;}
private:
    Block *next;

};

class Branch : public Block
{
public:
    virtual ~Branch() override{}
    virtual Block *execute() override {return nullptr;}
    virtual void setNext () override{}
};

class Terminal : public Block
{
public:
    virtual ~Terminal() override{}
    virtual Block *execute() override {return nullptr;}
    virtual void setNext () override{}
};

class FlowChart : public QWidget
{
    Q_OBJECT

protected:
    virtual void paintEvent(QPaintEvent *pEvent);
    virtual void mousePressEvent(QMouseEvent *pEvent);
    virtual void mouseMoveEvent(QMouseEvent *pEvent);
    virtual void mouseDoubleClickEvent(QMouseEvent * event);

private:
    Block *fRoot;
    double fZoom;
    int fStatus;
    virtual QSize sizeHint() const;
    QList<InsertionPoint> insertionPoints;
    InsertionPoint fTargetPoint;
    QString fBuffer;
    bool fMultiInsert;
    FlowChartStyle fStyle;
    QStack<QString> undoStack;
    QStack<QString> redoStack;

public:
    enum {Display, Selectable, Insertion, Changing};

    Block *fActiveBlock;

    FlowChart(QWidget *pObj = nullptr);
    ~FlowChart();
    QDomDocument document() const;
    InsertionPoint targetPoint() const { return fTargetPoint; }

    void paintTo(QPainter *canvas);

    bool isBlockActive(Block *block);

    bool isThereOutput(Block *block);

    void paint(QPainter *canvas, Block *block);

    Block * root() const;
    Block * activeBlock() const;
    double zoom() const;
    bool isRunning = false;
    int status() const { return fStatus; }
    void deleteBlock(Block *aBlock);
    InsertionPoint getNearistPoint(int x, int y) const;
    void regeneratePoints();
    void generatePoints(Block *aBlock); // recursive
    static double calcLength(const QPointF & p1, const QPointF & p2);
    QString buffer() const { return fBuffer; }
    bool multiInsert() const { return fMultiInsert; }
    FlowChartStyle chartStyle() const { return fStyle; }
    void setChartStyle(const FlowChartStyle & aStyle);
    static void drawBottomArrow(QPainter *canvas, const QPointF & aPoint, const QSizeF & aSize);
    static void drawRightArrow(QPainter *canvas, const QPointF & aPoint, const QSizeF & aSize);
    static void drawLeftArrow(QPainter *canvas, const QPointF & aPoint, const QSizeF & aSize);
    QDomElement varsToXML(QDomDocument & doc) const;
    void XMLToVars(const QDomElement & doc);
    QString toString();
    void fromString(const QString & str);
    bool canUndo() const;
    bool canRedo() const;
    bool canPaste() const;
    void makeChanged();
    void makeUndo();
signals:
    void zoomChanged(const double aZoom);
    void statusChanged();
    void editBlock(Block *block);
    void changed();
    void modified();

public slots:
    void clear();
    void selectAll();
    void deselectAll();
    void setZoom(const double aZoom);
    void setStatus(int aStatus);
    void deleteActiveBlock();
    void realignObjects();
    void setBuffer(const QString & aBuffer);
    void setMultiInsert(bool aValue) { fMultiInsert = aValue; }
    void undo();
    void redo();
};

void checkForNext (Block *block);
static Block *getBlockByID (int id){
    for (int i = 0; i < blocks.length(); i++) {
        Block *bk = blocks.at(i);
        if(bk->getAttribute("id").toInt() == id){
            return bk;
        }
    }
    return nullptr;
}

#endif // FLOWCHART_H
