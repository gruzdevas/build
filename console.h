/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#ifndef CONSOLE_H
#define CONSOLE_H

#include <QObject>
#include <QVariant>
#include <QDockWidget>
#include <QPlainTextEdit>
#include <QFrame>
#include <QScrollBar>
#include <QTextBlock>
#include <QDialog>
#include <QPushButton>


class Console : public QPlainTextEdit
{
    Q_OBJECT
public:
    QString prompt;
    Console();
    ~Console() {}
    QString read();
    void insertPrompt(bool insertNewBlock);
    void onEnter();
    bool isLocked;
    void scrollDown();


signals:
    void windowVisibilityChanged();
    void dataRecieved();

public slots:
    void output(QString s);
    void setupInput();
protected:
    virtual void keyPressEvent(QKeyEvent *);
    virtual void mousePressEvent(QMouseEvent *);
};

class ConsoleDialog : public QDialog
{
public:
    Console *console;
    ConsoleDialog();
};


extern ConsoleDialog *console;

#endif // CONSOLE_H
