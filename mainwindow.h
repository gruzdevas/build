/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "helpwindow.h"
#include "flowchart.h"
#include "console.h"
#include "globvars.h"

#include <QtGui>
#include <QMainWindow>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QToolButton>
#include <QAbstractItemView>
#include <QComboBox>
#include <QLabel>
#include <QApplication>
#include <QTextCodec>
#include <QTableWidget>

#include <QDebug>
#include <QTextStream>



QT_BEGIN_NAMESPACE
class QAction;
class QToolBox;
class QSpinBox;
class QComboBox;
class QFontComboBox;
class QButtonGroup;
class QLineEdit;
class QGraphicsTextItem;
class QFont;
class QToolButton;
class QAbstractButton;
class QGraphicsView;
class QTableWidget;
QT_END_NAMESPACE

class AlgScrollArea : public QScrollArea
{
  Q_OBJECT
  protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);
  signals:
    void mouseDown();
    void zoomStepped(int);
    void scrollStepped(int);
  public:
     explicit AlgScrollArea(QWidget* parent=nullptr) : QScrollArea(parent) { }
    ~AlgScrollArea() { }

};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
     MainWindow(QWidget *parent = nullptr, Qt::WindowFlags flags = nullptr);~MainWindow();
     FlowChart * document() const { return fDocument; }
     void setDocument(FlowChart * aDocument);
     static QString getFilterFor(const QString & fileExt);
     static QString getWriteFormatFilter();

private slots:
   void setNames();
   void slotRun();
   void slotStep();
   void slotStop();
   void slotStepByStep();
   void slotRunTest();
   void slotFileOpen();
   void slotFileNew();
   void slotFileSave();
   void slotFileSaveAs();
   void slotFileExport();
   void slotFileExportSVG();
   void slotFilePrint();
//   void slotEditCopy();
//   void slotEditPaste();
//   void slotEditCut();
   void slotAddVariable();
   void slotDeleteVariable();
   void slotEditVariable(QTableWidgetItem *item);
   void slotEditDelete();
   void slotHelpAbout();
   void slotHelpAboutQt();
   void slotChangeLeft();
   void slotChangeRight();
   void slotToolArrow();
   void slotToolProcess();
   void slotToolIf();
   void slotToolIo();
   void slotToolOu();
   void slotDocumentSaved();
   void slotDocumentChanged();
   void slotDocumentLoaded();

   void setZoom(int quarts);
   void shiftZoom(int step);
   void shiftScrollY(int step);
   void slotStatusChanged();
   void slotEditBlock(Block *aBlock);
   void updateActions();
   void docToolsVisibilityChanged(bool visible);

   void slotOpenDocument(const QString &fn);
signals:
    void documentLoaded();
    void documentSaved();
    void documentChanged();
    void documentUnloaded();
private:
    FlowChart *fDocument;
    Block *activeBlock;
    bool isSaved;
    QVBoxLayout *fVLayout;
    QHBoxLayout *fHLayout;
    AlgScrollArea *saScheme;
    QDockWidget *dockTools;
    QDockWidget *dockVariable;
    QTableWidget *variableTable;
    QFrame *toolsWidget;
    QToolButton *tbArrow;
    QToolButton *tbProcess;
    QToolButton *tbIf;
    QToolButton *tbIo;
    QToolButton *tbOu;

    QAction *actNew;
    QAction *actOpen;
    QAction *actSave;
    QAction *actSaveAs;
    QAction *actExit;
    QAction *actUndo;
    QAction *actRedo;
//    QAction *actCut;
//    QAction *actCopy;
//    QAction *actPaste;
    QAction *actDelete;
    QAction *actHelp;
    QAction *actRun;
    QAction *actStepByStep;
    QAction *actTest;
    QAction *actStep;
    QAction *actStop;
    QAction *actAbout;
    QAction *actAboutQt;
    QAction *actExport;
    QAction *actExportSVG;
    QAction *actPrint;
    QAction *actVariables;
    QAction *actTools;
    QAction *actCode;
    QAction *acteng;
    QAction *actrus;
    QList<QAction *> actLanguages;

    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuHelp;
    QMenu *menuWindow;
    QMenu *menuStart;
    QString fileName;
    QToolBar *toolBar;
    QToolBar *tBar;
    QLabel *zoomLabel;
    QLabel *labelMenu;
    QSlider *zoomSlider;
    QDialog *desicionDialog;

    HelpWindow *helpWindow;

    QStack<QString> undoStack;
    QStack<QString> redoStack;

    void setupUi();
    void readSettings();
    void writeSettings();
    void createMenu();
    void updateVariableTable();
    void createActions();
    void createToolbox();
    void createToolBar();
    bool okToContinue();
  protected:
    void closeEvent(QCloseEvent *event);
};


#endif // MAINWINDOW_H
