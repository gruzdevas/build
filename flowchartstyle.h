/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#ifndef FLOWCHARTSTYLE_H
#define FLOWCHARTSTYLE_H

#include <QtGui>

class FlowChartStyle
{
  private:
    QColor fNormalBackground;
    QColor fNormalForeground;
    QColor fSelectedBackground;
    QColor fSelectedForeground;
    double fLineWidth;
    QColor fNormalMarker;
    QColor fSelectedMarker;
    double fFontSize;

  public:
    FlowChartStyle();
    ~FlowChartStyle() {}

    QColor normalBackground() const { return fNormalBackground; }
    QColor normalForeground() const { return fNormalForeground; }
    QColor selectedBackground() const { return fSelectedBackground; }
    QColor selectedForeground() const { return fSelectedForeground; }
    double lineWidth() const { return fLineWidth; }
    QColor normalMarker() const { return fNormalMarker; }
    QColor selectedMarker() const { return fSelectedMarker; }
    double fontSize() const { return fFontSize; }

    void setNormalBackground(const QColor & aValue) { fNormalBackground = aValue; }
    void setNormalForeground(const QColor & aValue) { fNormalForeground = aValue; }
    void setSelectedBackground(const QColor & aValue) { fSelectedBackground = aValue; }
    void setSelectedForeground(const QColor & aValue) { fSelectedForeground = aValue; }
    void setLineWidth(const double aValue) { fLineWidth = aValue; }
    void setNormalMarker(const QColor & aValue) { fNormalMarker = aValue; }
    void setSelectedMarker(const QColor & aValue) { fSelectedMarker = aValue; }
    void setFontSize(const double aValue) { fFontSize = aValue; }
};

Q_DECLARE_TYPEINFO(FlowChartStyle, Q_MOVABLE_TYPE);

#endif // FLOWCHARTSTYLE_H
