/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#include "console.h"
#include <QDebug>
#include <QVBoxLayout>


Console::Console()
{
    QPalette p = palette();
    p.setColor(QPalette::Base, Qt::black);
    p.setColor(QPalette::Text, Qt::green);
    setPalette(p);
}

void Console::mousePressEvent(QMouseEvent *)
{
    setFocus();
}

void Console::keyPressEvent(QKeyEvent *event)
{
    // …
    if(isLocked)
        return;
    if(event->key() >= 0x20 && event->key() <= 0x7e
       && (event->modifiers() == Qt::NoModifier || event->modifiers() == Qt::ShiftModifier))
        QPlainTextEdit::keyPressEvent(event);
    // …
    if(event->key() == Qt::Key_Backspace
       && event->modifiers() == Qt::NoModifier
       && textCursor().positionInBlock() > prompt.length())
        QPlainTextEdit::keyPressEvent(event);

    if(event->key() == Qt::Key_Return && event->modifiers() == Qt::NoModifier)
        onEnter();
}

void Console::onEnter()
{
    if(textCursor().positionInBlock() == prompt.length())
    {
        insertPrompt(true);
        return;
    }
    QString cmd = textCursor().block().text().mid(prompt.length());
    this->clearFocus();
    emit dataRecieved();
    //isLocked = true;
}

void Console::insertPrompt(bool insertNewBlock)
{
    if(insertNewBlock)
        textCursor().insertBlock();
    textCursor().insertText(prompt);
    QTextCharFormat format;
    format.setForeground(Qt::green);
    textCursor().setBlockCharFormat(format);
    scrollDown();
}

void Console::scrollDown()
{
    QScrollBar *vbar = verticalScrollBar();
    vbar->setValue(vbar->maximum());
}

void Console::output(QString s)
{
    QTextCharFormat format;
    format.setForeground(Qt::white);
    textCursor().setBlockCharFormat(format);
    textCursor().insertBlock();
    prompt = "output> ";
    insertPrompt(false);
    textCursor().insertText(s);
    //insertPrompt(true);
}

void Console::setupInput()
{
    QTextCharFormat format;
    format.setForeground(Qt::white);
    textCursor().setBlockCharFormat(format);
    textCursor().insertBlock();
    prompt = "input> ";
    insertPrompt(false);
}

QString Console::read()
{
    return textCursor().block().text().mid(prompt.length());
}

ConsoleDialog::ConsoleDialog(){
    this->setWindowTitle("Консоль"); 
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->console = new Console();
    this->console->isLocked = false;
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(this->console);
    this->setLayout(mainLayout);
    connect(this->console, SIGNAL(dataRecieved()), this, SLOT(accept()));
}
