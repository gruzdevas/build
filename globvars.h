/****************************************************************************
**                                                                         **
** Copyright (C) 2009-2014 Victor Zinkevich.                               **
** Copyright (C) 2019 Anton Gruzdev.                                       **
**                                                                         **
** This file is part of the Algo.                                          **
**                                                                         **
** Algo is free software: you can redistribute it and/or                   **
** modify it under the terms of the GNU General Public License as          **
** published by the Free Software Foundation, either version 3 of the      **
** License, or (at your option) any later version.                         **
**                                                                         **
** Algo is distributed in the hope that it will be useful,                 **
** but WITHOUT ANY WARRANTY; without even the implied warranty of          **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
** GNU General Public License for more details.                            **
**                                                                         **
** You should have received a copy of the GNU General Public License       **
** along with Algo.                                                        **
** If not, see <https://www.gnu.org/licenses/>.                            **
**                                                                         **
****************************************************************************/

#ifndef VARIABLES_H
#define VARIABLES_H

#include <QList>
class Variable
{
private:
    QString type;
    QString name;
    QString value;
public:
    QString getName(){return this->name;}
    void setName(QString newName) {this->name = newName;}
    QString getType(){return this->type;}
    void setType(QString newType) {this->type = newType;}
    QString getValue(){return this->value;}
    void setValue(QString newValue) {this->value = newValue;}
};

extern QList <Variable *> variables;


//class Variable
//{
//private:
//    QString name;
//public:
//    virtual ~IVariable(){}
//    virtual void setValue(QString newValue) = 0;
//    QString getName();
//    void setName(QString newName) {this->name = newName;}
//};

//class Number : public IVariable
//{
//private:
//    double value;
//public:
//    double getValue();
//    void setValue(QString newValue) override
//    {
//        bool flag;
//        double secondValue = newValue.toDouble(&flag);
//        if(flag){this->value = secondValue;}
//    }
//};

//class String : public IVariable
//{
//private:
//    QString value;
//public:
//    QString getValue();
//    void setValue(QString newName) override {this->value = newName;}
//};

//extern QList <IVariable *> variables;

#endif // VARIABLES_H
